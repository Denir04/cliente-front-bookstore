export const genderOptions = [
  { value: "male", name: "Masculino" },
  { value: "female", name: "Feminino" },
  { value: "other", name: "Outro" },
];

export const residentOptions = [
  { value: "house", name: "Casa" },
  { value: "apartment", name: "Apartamento" },
  { value: "condominium", name: "Condominio" },
];

export const streetOptions = [
  { value: "road", name: "Rua" },
  { value: "avenue", name: "Avenida" },
  { value: "garden", name: "Jardim" },
  { value: "highway", name: "Rodovia" },
];

export const brandOptions = [
  { value: "MASTERCARD", name: "Mastercard" },
  { value: "VISA", name: "Visa" },
  { value: "ELO", name: "Elo" },
];
