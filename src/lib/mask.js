export const normalizePhoneNumber = (value) => {
  if (!value) return "";
  return value
    .replace(/[\D]/g, "")
    .replace(/(\d{2})(\d)/, "($1) $2")
    .replace(/(\d{9})(\d+?)/, "$1");
};

export const normalizeCpfNumber = (value) => {
  if (!value) return "";
  return value
    .replace(/[\D]/g, "")
    .replace(/(\d{3})(\d)/, "$1.$2")
    .replace(/(\d{3})(\d)/, "$1.$2")
    .replace(/(\d{3})(\d)/, "$1-$2")
    .replace(/(-\d{2})\d+?$/, "$1");
};

export const normalizeCepNumber = (value) => {
  if (!value) return "";
  return value
    .replace(/[\D]/g, "")
    .replace(/(\d{5})(\d)/, "$1-$2")
    .replace(/(-\d{3})\d+?$/, "$1");
};

export const normalizeCreditCardNumber = (value) => {
  if (!value) return "";
  return value
    .replace(/[\D]/g, "")
    .replace(/(\d{4})(\d)/, "$1.$2")
    .replace(/(\d{4})(\d)/, "$1.$2")
    .replace(/(\d{4})(\d)/, "$1.$2")
    .replace(/(\d{4})(\d)/, "$1");
};

export const normalizeCodSeguranca = (value) => {
  if (!value) return "";
  return value.replace(/[\D]/g, "").replace(/(\d{3})(\d)/, "$1");
};

export const convertDate = (value) => {
  if (!value) return "";
  if (value.length < 10) return null;
  const arr = value.split("-");
  const dateFormatted = `${arr[2]}-${arr[1]}-${arr[0]}`;
  return dateFormatted;
};

export const unconvertDate = (value) => {
  if (!value) return "";
  if (value.length < 10) return null;
  const arr = value.split("-");
  const dateFormatted = `${arr[2]}-${arr[1]}-${arr[0]}`;
  return dateFormatted;
};
