import React, { useEffect, useState } from "react";

import Account from "../..";
import Input from "../../../../components/Input";
import Button from "../../../../components/Button";

import styles from "./styles";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import dlv from "dlv";
import { basicCustomerSchema } from "../../../../schemas";
import { normalizeCpfNumber, normalizePhoneNumber } from "../../../../lib/mask";
import Select from "../../../../components/Select";
import { genderOptions } from "../../../../lib/selectOptions";
import { getCustomer, updateCustomer } from "../../../../api/customer";
import SuccessMsg from "../../../../components/SuccessMsg";

export default function EditMyAccount() {
  const [loading, setLoading] = useState(true);
  const [customer, setCustomer] = useState({});
  const [success, setSuccess] = useState(null);
  const router = useRouter();

  useEffect(() => {
    (async function () {
      try {
        const backCustomer = await getCustomer(1);
        setCustomer(backCustomer.data);
      } catch (error) {
        console.log(`deu ruim`);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const onSubmit = async (values, actions) => {
    try {
      await updateCustomer(1, values);
      setSuccess(true);
    } catch (error) {
      console.log("deu erro");
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    handleChange,
    handleBlur,
  } = useFormik({
    initialValues: {
      customerName: dlv(customer, "customerName"),
      customerDocumentNumber: dlv(customer, "customerDocumentNumber"),
      customerBirthDate: dlv(customer, "customerBirthDate"),
      customerGender: dlv(customer, "customerGender"),
      customerPhone: dlv(customer, "customerPhone"),
      customerEmail: dlv(customer, "customerEmail"),
    },
    enableReinitialize: true,
    validationSchema: basicCustomerSchema,
    onSubmit,
  });

  if (loading)
    return (
      <Account>
        <p>Loading</p>
      </Account>
    );

  if (success)
    return (
      <div className="success-container">
        <style jsx>{styles}</style>
        <SuccessMsg onClick={() => router.push("/conta/minha-conta")} />
      </div>
    );

  return (
    <Account>
      <div className="page-my-account">
        <h1>Editar Minha Conta</h1>
        <style jsx>{styles}</style>
        <form onSubmit={handleSubmit}>
          <article>
            <header>
              <h2>Dados Básicos</h2>
            </header>
            <div className="basic-data-inputs">
              <div className="row">
                <Input
                  label="Nome Completo"
                  value={values.customerName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="customerName"
                  errorMsg={touched.customerName ? errors.customerName : ""}
                />
              </div>
              <div className="row">
                <Input
                  label="CPF"
                  value={values.customerDocumentNumber}
                  onChange={(e) => {
                    e.target.value = normalizeCpfNumber(e.target.value);
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  name="customerDocumentNumber"
                  errorMsg={
                    touched.customerDocumentNumber
                      ? errors.customerDocumentNumber
                      : ""
                  }
                />
                <Input
                  label="Data de Nascimento"
                  type="date"
                  value={values.customerBirthDate}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="customerBirthDate"
                  errorMsg={
                    touched.customerBirthDate ? errors.customerBirthDate : ""
                  }
                />
              </div>
              <div className="row">
                <Select
                  label={"Genêro"}
                  name={"customerGender"}
                  value={values.customerGender}
                  options={genderOptions}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.customerGender ? errors.customerGender : ""}
                />
                <Input
                  label="Telefone"
                  value={values.customerPhone}
                  onChange={(e) => {
                    e.target.value = normalizePhoneNumber(e.target.value);
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  name="customerPhone"
                  errorMsg={touched.customerPhone ? errors.customerPhone : ""}
                />
              </div>
              <div className="row">
                <Input
                  name="customerEmail"
                  label="E-mail"
                  value={values.customerEmail}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.customerEmail ? errors.customerEmail : ""}
                />
              </div>
            </div>
            <div className="actions">
              <Button
                textButton="Voltar"
                className="secondary"
                onClick={() => router.back()}
              />
              <Button textButton="Editar" disabled={isSubmitting} />
            </div>
          </article>
        </form>
      </div>
    </Account>
  );
}
