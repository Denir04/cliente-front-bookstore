import css from "styled-jsx/css";

export default css`
  .success-container {
    margin-top: 200px;
  }

  .page-my-account form {
    border: 1px solid var(--color-zero);
    padding: 18px;
    background-color: var(--color-secondary);
  }

  .page-my-account h1 {
    margin-bottom: 20px;
  }

  .page-my-account form article {
    margin-bottom: 15px;
  }

  .page-my-account form article header {
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid var(--color-zero);
    margin-bottom: 20px;
  }

  .page-my-account form article header h2 {
    font-size: 1.2rem;
    font-weight: bold;
    margin-bottom: 10px;
  }

  .page-my-account form article header span {
    cursor: pointer;
  }

  .page-my-account form article .basic-data-inputs,
  .page-my-account form article .residential-address-inputs,
  .page-my-account form article .login-data-inptus {
    display: flex;
    flex-direction: column;
    gap: 10px;
  }

  .page-my-account form article .basic-data-inputs .row:nth-child(2),
  .page-my-account form article .basic-data-inputs .row:nth-child(3) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 20px;
  }

  .actions {
    margin-top: 40px;
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 20px;
  }
`;
