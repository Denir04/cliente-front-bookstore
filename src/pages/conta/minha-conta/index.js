import React, { useEffect, useState } from "react";
import Account from "..";

import styles from "./styles";

import Input from "../../../components/Input";
import Select from "../../../components/Select";
import { useRouter } from "next/router";
import { getCustomer } from "../../../api/customer";
import dlv from "dlv";
import { genderOptions } from "../../../lib/selectOptions";

export default function MyAccount() {
  const [customer, setCustomer] = useState({});
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  useEffect(() => {
    (async function () {
      try {
        const backCustomer = await getCustomer(1);
        setCustomer(backCustomer.data);
      } catch (error) {
        console.log(`deu ruim`);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  if (loading)
    return (
      <Account>
        <p>Loading</p>
      </Account>
    );

  return (
    <Account>
      <div className="page-my-account">
        <h1>Minha Conta</h1>
        <style jsx>{styles}</style>
        <form onSubmit={handleSubmit}>
          <article>
            <header>
              <h2>Dados Básicos</h2>
              <span onClick={() => router.push("/conta/minha-conta/editar")}>
                Editar
              </span>
            </header>
            <div className="basic-data-inputs">
              <div className="row">
                <Input
                  label="Nome Completo"
                  value={dlv(customer, "customerName")}
                  disabled
                />
              </div>
              <div className="row">
                <Input
                  label="CPF"
                  value={dlv(customer, "customerDocumentNumber")}
                  disabled
                />
                <Input
                  label="Data de Nascimento"
                  type="date"
                  value={dlv(customer, "customerBirthDate")}
                  disabled
                />
              </div>
              <div className="row">
                <Select
                  label="Gênero"
                  options={genderOptions}
                  value={dlv(customer, "customerGender")}
                  disabled
                />
                <Input
                  label="Telefone"
                  value={dlv(customer, "customerPhone")}
                  disabled
                />
              </div>
              <div className="row">
                <Input
                  label="E-mail"
                  value={dlv(customer, "customerEmail")}
                  disabled
                />
              </div>
              <p
                className="change-password"
                onClick={() => router.push("/conta/minha-conta/editar-senha")}
              >
                Alterar Senha
              </p>
            </div>
          </article>
        </form>
      </div>
    </Account>
  );
}
