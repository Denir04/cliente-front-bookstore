import React, { useState } from "react";
import { useRouter } from "next/router";

import Account from "../..";
import Button from "../../../../components/Button";
import Input from "../../../../components/Input";

import styles from "./styles";
import { useFormik } from "formik";
import { passwordsSchema } from "../../../../schemas";
import SuccessMsg from "../../../../components/SuccessMsg";
import { updateCustomer } from "../../../../api/customer";

export default function EditMyPassword() {
  const router = useRouter();
  const [success, setSuccess] = useState(null);

  const onSubmit = async (values, actions) => {
    try {
      await updateCustomer(1, values);
      setSuccess(true);
    } catch (error) {
      console.log("deu erro");
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleSubmit,
    handleChange,
    handleBlur,
  } = useFormik({
    initialValues: {
      customerPassword: "",
      customerConfirmPassword: "",
    },
    validationSchema: passwordsSchema,
    onSubmit,
  });

  if (success)
    return (
      <div className="success-container">
        <style jsx>{styles}</style>
        <SuccessMsg onClick={() => router.push("/conta/minha-conta")} />
      </div>
    );

  return (
    <Account>
      <div className="page-my-account">
        <h1>Editar Senha</h1>
        <style jsx>{styles}</style>
        <form onSubmit={handleSubmit}>
          <article>
            <header>
              <h2>Senha</h2>
            </header>
            <div className="basic-data-inputs">
              <div className="row">
                <Input
                  label="Nova Senha"
                  value={values.customerPassword}
                  type="password"
                  name="customerPassword"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.customerPassword ? errors.customerPassword : ""
                  }
                />
              </div>
              <div className="row">
                <Input
                  label="Repita Nova Senha"
                  value={values.customerConfirmPassword}
                  type="password"
                  name="customerConfirmPassword"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.customerConfirmPassword
                      ? errors.customerConfirmPassword
                      : ""
                  }
                />
              </div>
            </div>
            <div className="actions">
              <Button
                textButton="Voltar"
                className="secondary"
                onClick={() => router.back()}
              />
              <Button textButton="Editar" disabled={isSubmitting} />
            </div>
          </article>
        </form>
      </div>
    </Account>
  );
}
