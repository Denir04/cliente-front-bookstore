import React, { useEffect, useState } from "react";
import Account from "..";

import styles from "./styles";
import RequestCard from "../../../components/RequestCard";
import { useRouter } from "next/router";
import { getMyOrders } from "../../../api/order";
import { formatDate } from "../../../lib/utils";

export default function MyRequests() {
  const router = useRouter();
  const [myOrders, setMyOrders] = useState([]);

  useEffect(() => {
    (async function () {
      try {
        const response = await getMyOrders(1);
        console.log(response.data);
        setMyOrders(response.data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  return (
    <Account>
      <div className="page-my-requests">
        <style jsx>{styles}</style>
        <h1>Meus Pedidos</h1>
        <section className="requests">
          {myOrders?.map((order) => (
            <RequestCard
              key={order["orderId"]}
              id={order["orderId"]}
              status={order["orderStatus"]}
              requestDate={formatDate(order["orderDate"])}
              itemsValue="50,00"
              totalValue="44,00"
              onView={() =>
                router.push(
                  `/conta/meus-pedidos/visualizar/${order["orderId"]}`
                )
              }
            />
          ))}
        </section>
      </div>
    </Account>
  );
}
