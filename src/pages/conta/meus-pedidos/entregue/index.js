import React from "react";
import { useRouter } from "next/router";

import styles from "./styles";
import GoBack from "../../../../components/GoBack";
import SelectAddressCard from "../../../../components/SelectAddressCard";
import SelectCreditCard from "../../../../components/SelectCreditCard";
import SelectedProductCard from "../../../../components/SelectedProductCard";
import Button from "../../../../components/Button";
import img1984 from "../../../../static/1984.jpg";

export default function ViewRequest() {
  const router = useRouter();

  return (
    <div className="view-request">
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <h1>N° Pedido Compra #22893824943245</h1>
      <article className="request-card-full">
        <header>
          <p>Data do Pedido: 20/08/2023</p>
          <p className="status">Entegue</p>
        </header>
        <section className="details">
          <article>
            <p>Valor dos Itens</p>
            <p>R$ 72,00</p>
          </article>
          <article>
            <p>Frete</p>
            <p>R$ 10,00</p>
          </article>
        </section>
        <footer>
          <p>Valor Final</p>
          <p>R$ 82,00</p>
        </footer>
      </article>
      <section className="addresses-n-payment">
        <article>
          <header>Endereço de Entrega</header>
          <div className="content">
            <SelectAddressCard value="minha-casa" selected />
          </div>
        </article>
        <article>
          <header>Endereço de Cobrança</header>
          <div className="content">
            <SelectAddressCard value="minha-casa" selected />
          </div>
        </article>
        <article>
          <header>Forma de Pagamento</header>
          <div className="content">
            <SelectCreditCard
              selected
              brand="Mastercard"
              lastNumbers="4390"
              name="mastercard-4390"
              creditValue="10,00"
            />
          </div>
        </article>
      </section>
      <section className="list-products">
        <h2>Produtos</h2>
        <div className="products-container">
          <article className="product">
            <SelectedProductCard
              imgUrl={img1984}
              title="1984"
              price="30,00"
              amount={1}
            />
          </article>
        </div>
      </section>
      <div className="action">
        <Button textButton="Efetuar uma troca" />
      </div>
    </div>
  );
}
