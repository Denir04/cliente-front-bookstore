import css from "styled-jsx/css";

export default css`
  .view-request h1 {
    margin-bottom: 20px;
  }

  .request-card-full {
    border: 1px solid var(--color-zero);
    background-color: var(--color-secondary);
    margin-bottom: 30px;
  }

  .request-card-full header {
    border-bottom: 1px solid var(--color-zero);
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 20px;
    height: 35px;
  }

  .request-card-full header .status {
    font-size: 1.1rem;
    font-weight: bold;
  }

  .request-card-full .details {
    padding: 15px 20px 10px;
  }

  .request-card-full .details article {
    display: flex;
    justify-content: space-between;
    margin-bottom: 5px;
  }

  .request-card-full footer {
    display: flex;
    padding: 15px 20px;
    justify-content: space-between;
    border-top: 1px solid var(--color-zero);
  }

  .addresses-n-payment {
    display: flex;
    justify-content: space-between;
    margin-bottom: 20px;
  }

  .addresses-n-payment article {
    border: 1px solid var(--color-zero);
    background-color: var(--color-secondary);
  }

  .addresses-n-payment article header {
    border-bottom: 1px solid var(--color-zero);
    padding: 5px 15px;
  }

  .addresses-n-payment article .content {
    padding: 0 15px;
  }

  .list-products h2 {
    font-size: 1.3rem;
    font-weight: bold;
    margin-bottom: 10px;
  }

  .list-products .products-container {
    display: flex;
    gap: 15px;
  }

  .list-products .products-container .product {
    padding: 5px 15px;
    border: 1px solid var(--color-zero);
    background-color: var(--color-secondary);
    min-width: fit-content;
    width: 300px;
  }

  .action {
    margin-top: 30px;
    width: 15%;
  }
`;
