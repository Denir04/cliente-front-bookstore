import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import styles from "./styles";
import GoBack from "../../../../components/GoBack";
import SelectAddressCard from "../../../../components/SelectAddressCard";
import SelectCreditCard from "../../../../components/SelectCreditCard";
import SelectedProductCard from "../../../../components/SelectedProductCard";

import imgLittlePrince from "../../../../static/pequeno-principe.jpg";
import img1984 from "../../../../static/1984.jpg";
import { getMyOrderById } from "../../../../api/order";
import { formatDate } from "../../../../lib/utils";

export default function ViewRequest() {
  const router = useRouter();
  const [order, setOrder] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async function () {
      try {
        const orderBack = await getMyOrderById(1, router.query.id);
        console.log(orderBack.data);
        setOrder(orderBack.data);
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  if (loading) return <p>Carregando...</p>;

  const totalValue = order["orderProducts"]?.reduce(function (total, product) {
    return (
      total + product["orderProductPrice"] * product["orderProductQuantity"]
    );
  }, 0);

  return (
    <div className="view-request">
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <h1>N° Pedido Compra #{order["orderId"]}</h1>
      <article className="request-card-full">
        <header>
          <p>Data do Pedido: {formatDate(order["orderDate"])}</p>
          <p className="status">{order["orderStatus"]}</p>
        </header>
        <section className="details">
          <article>
            <p>Valor dos Itens</p>
            <p>R$ {totalValue.toFixed(2)}</p>
          </article>
          {order["orderShippingValue"] && (
            <article>
              <p>Frete</p>
              <p>R$ {order["orderShippingValue"].toFixed(2)}</p>
            </article>
          )}
        </section>
        <footer>
          <p>Valor Final</p>
          <p>R$ {(totalValue + order["orderShippingValue"]).toFixed(2)}</p>
        </footer>
      </article>
      <section className="addresses-n-payment">
        <article>
          <header>Endereço de Entrega</header>
          <div className="content">
            <SelectAddressCard
              nickname={order["orderShippingAddress"]?.nickname}
              neighborhood={order["orderShippingAddress"]?.neighborhood}
              typeStreet={order["orderShippingAddress"]?.typeStreet}
              street={order["orderShippingAddress"]?.street}
              number={order["orderShippingAddress"]?.number}
              selected
            />
          </div>
        </article>
        <article>
          <header>Endereço de Cobrança</header>
          <div className="content">
            <SelectAddressCard
              nickname={order["orderBillingAddress"]?.nickname}
              neighborhood={order["orderBillingAddress"]?.neighborhood}
              typeStreet={order["orderBillingAddress"]?.typeStreet}
              street={order["orderBillingAddress"]?.street}
              number={order["orderBillingAddress"]?.number}
              selected
            />
          </div>
        </article>
        <article>
          <header>Forma de Pagamento</header>
          <div className="content">
            {order["orderPaymentCard"]?.map((card) => (
              <SelectCreditCard
                selected
                key={card["paymentCardId"]}
                brand={card["brand"]}
                lastNumbers={card["number"].slice(-4)}
                name={`${card["brand"]}-${card["number"].slice(-4)}`}
                creditValue={card["price"].toFixed(2)}
              />
            ))}
          </div>
        </article>
      </section>
      <section className="list-products">
        <h2>Produtos</h2>
        <div className="products-container">
          <article className="product">
            {order["orderProducts"]?.map((product) => (
              <SelectedProductCard
                key={product["orderProductId"]}
                imgUrl={product["orderProductImageUrl"]}
                price={product["orderProductPrice"]}
                amount={product["orderProductQuantity"]}
              />
            ))}
          </article>
        </div>
      </section>
    </div>
  );
}
