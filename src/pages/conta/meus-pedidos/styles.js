import css from "styled-jsx/css";

export default css`
  .page-my-requests h1 {
    margin-bottom: 20px;
  }

  .page-my-requests .requests {
    display: flex;
    flex-direction: column;
    gap: 15px;
  }
`;
