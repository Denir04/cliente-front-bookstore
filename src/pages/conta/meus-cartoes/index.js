import React, { useEffect, useState } from "react";
import Account from "..";
import styles from "./styles";
import CreditCard from "../../../components/CreditCard";
import Button from "../../../components/Button";
import { useRouter } from "next/router";
import { getCreditCards } from "../../../api/customer";
import dlv from "dlv";

export default function MyCreditCards() {
  const [creditCards, setCreditCards] = useState([]);
  const router = useRouter();

  useEffect(() => {
    (async function () {
      try {
        const creditCardsBack = await getCreditCards(1);
        setCreditCards(creditCardsBack.data);
      } catch (error) {
        console.log(`deu ruim - getCreditCards`);
      }
    })();
  }, []);

  return (
    <Account>
      <h1>Meus Cartões</h1>
      <style jsx>{styles}</style>
      <div className="credit-card-list">
        {creditCards.map((card) => (
          <CreditCard
            key={dlv(card, "cardId")}
            id={dlv(card, "cardId")}
            lastNumbers={dlv(card, "cardNumber")}
            brand={dlv(card, "cardBrand")}
            preferential={dlv(card, "cardPreference")}
            router={router}
          />
        ))}
      </div>
      <Button
        textButton="Registrar Novo Cartão"
        onClick={() => router.push("/conta/meus-cartoes/adicionar")}
      />
    </Account>
  );
}
