import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import Button from "../../../../components/Button";
import { getCreditCards, updateCreditCard } from "../../../../api/customer";
import dlv from "dlv";
import styles from "./styles";

export default function EditCreditCard() {
  const router = useRouter();
  const [creditCard, setCreditCard] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async function () {
      try {
        const creditCardsBack = await getCreditCards(1);
        const creditCard = creditCardsBack.data.filter(
          (card) => card.cardId === Number(router.query.id)
        );
        setCreditCard(...creditCard);
      } catch (error) {
        console.log(`deu ruim`);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const handleEdit = async () => {
    try {
      await updateCreditCard(1, router.query.id);
      router.back();
    } catch (error) {
      console.log(`deu ruim - update card`);
    }
  };

  if (loading) return <p>Loading</p>;

  const finalNumbers = dlv(creditCard, "cardNumber", "");

  return (
    <div className="edit-credit-card">
      <style jsx>{styles}</style>
      <p>
        Você realmente configurar como preferencial o{" "}
        {dlv(creditCard, "cardBrand")} ****{" "}
        {finalNumbers.substr(finalNumbers.length - 4)}?
      </p>
      <div className="container-buttons">
        <Button
          textButton="Voltar"
          className="secondary"
          onClick={() => router.back()}
        />
        <Button
          textButton="Configurar"
          className="forth"
          onClick={handleEdit}
        />
      </div>
    </div>
  );
}
