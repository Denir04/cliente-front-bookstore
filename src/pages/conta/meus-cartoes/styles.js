import css from "styled-jsx/css";

export default css`
  .credit-card-list {
    display: flex;
    flex-direction: column;
    gap: 10px;
    margin: 20px 0 20px;
  }
`;
