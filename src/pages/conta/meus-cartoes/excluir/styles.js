import css from "styled-jsx/css";

export default css`
  .delete-credit-card {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 10px;
    margin-top: 200px;
  }

  .delete-credit-card p {
    font-size: 1.2rem;
  }

  .container-buttons {
    display: flex;
    gap: 15px;
    width: 200px;
  }
`;
