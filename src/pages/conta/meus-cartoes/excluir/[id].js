import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { deleteCreditCard, getCreditCards } from "../../../../api/customer";
import styles from "./styles";
import dlv from "dlv";
import Button from "../../../../components/Button";

export default function DeleteCreditCard() {
  const router = useRouter();
  const [creditCard, setCreditCard] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    (async function () {
      try {
        const creditCardsBack = await getCreditCards(1);
        const creditCard = creditCardsBack.data.filter(
          (card) => card.cardId === Number(router.query.id)
        );
        setCreditCard(...creditCard);
      } catch (error) {
        console.log(`deu ruim`);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const handleDelete = async () => {
    try {
      await deleteCreditCard(1, router.query.id);
      router.back();
    } catch (error) {
      if (error.code === "ERR_BAD_REQUEST") setError(true);
      console.log(`deu ruim - delete`);
    }
  };

  if (loading) return <p>Loading</p>;

  if (error)
    return (
      <div className="delete-credit-card">
        <style jsx>{styles}</style>
        <p>Não é possível excluir um cartão como preferencial</p>
        <div className="container-buttons">
          <Button
            textButton="Voltar"
            className="secondary"
            onClick={() => router.back()}
          />
        </div>
      </div>
    );

  const finalNumbers = dlv(creditCard, "cardNumber", "");

  return (
    <div className="delete-credit-card">
      <style jsx>{styles}</style>
      <p>
        Você realmente deseja excluir o {dlv(creditCard, "cardBrand")} ****{" "}
        {finalNumbers.substr(finalNumbers.length - 4)}?
      </p>
      <div className="container-buttons">
        <Button
          textButton="Voltar"
          className="secondary"
          onClick={() => router.back()}
        />
        <Button
          textButton="Excluir"
          className="terciary"
          onClick={handleDelete}
        />
      </div>
    </div>
  );
}
