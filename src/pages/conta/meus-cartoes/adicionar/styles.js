import css from "styled-jsx/css";

export default css`
  .success-container {
    margin-top: 200px;
  }

  .page-new-credit-card form {
    border: 1px solid var(--color-zero);
    padding: 18px;
    background-color: var(--color-secondary);
  }

  .page-new-credit-card h1 {
    margin-bottom: 20px;
  }

  .page-new-credit-card form article {
    margin-bottom: 15px;
  }

  .page-new-credit-card form article header {
    font-size: 1.2rem;
    font-weight: bold;
    margin-bottom: 20px;
    border-bottom: 2px solid var(--color-zero);
  }

  .page-new-credit-card form article .credit-card-inputs {
    display: flex;
    flex-direction: column;
    gap: 5px;
  }

  .page-new-credit-card form article .credit-card-inputs .row {
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 20px;
  }
`;
