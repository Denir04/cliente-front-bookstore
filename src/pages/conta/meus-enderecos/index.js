import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Account from "..";
import styles from "./styles";
import AddressCard from "../../../components/AddressCard";
import Button from "../../../components/Button";
import { getCustomerAddresses } from "../../../api/customer";
import dlv from "dlv";

export default function MyAddresses() {
  const [addresses, setAddresses] = useState([]);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  useEffect(() => {
    (async function () {
      try {
        const addressesBack = await getCustomerAddresses(1);
        setAddresses(addressesBack.data);
      } catch (error) {
        console.log(`deu ruim`);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  if (loading)
    return (
      <Account>
        <p>Loading</p>
      </Account>
    );

  return (
    <Account>
      <h1>Meus Endereços</h1>
      <style jsx>{styles}</style>
      <div className="address-list">
        {addresses.map((address) => (
          <AddressCard
            router={router}
            key={dlv(address, "addressId")}
            id={dlv(address, "addressId")}
            nickname={dlv(address, "addressNickname")}
            neighborhood={dlv(address, "addressNeighborhood")}
            street={dlv(address, "addressStreet")}
            number={dlv(address, "addressNumber")}
            typeStreet={dlv(address, "addressTypeStreet")}
            defaultBilling={dlv(address, "addressBillingDefault")}
            defaultDelivery={dlv(address, "addressDeliveryDefault")}
            defaultResident={dlv(address, "addressResidentDefault")}
          />
        ))}
      </div>
      <Button
        textButton="Registrar Novo Endereço"
        onClick={() => router.push(`/conta/meus-enderecos/adicionar`)}
      />
    </Account>
  );
}
