import React from "react";
import Account from "../..";
import Input from "../../../../components/Input";
import { useFormik } from "formik";
import { addressSchema } from "../../../../schemas";
import styles from "./styles";
import Select from "../../../../components/Select";
import { residentOptions, streetOptions } from "../../../../lib/selectOptions";
import { normalizeCepNumber } from "../../../../lib/mask";
import Button from "../../../../components/Button";
import { createNewAddress } from "../../../../api/customer";
import { useRouter } from "next/router";
import Checkbox from "../../../../components/Checkbox";

export default function RegisterNewAddress() {
  const router = useRouter();

  const onSubmit = async (values, actions) => {
    try {
      await createNewAddress(1, values);
      router.back();
    } catch (error) {
      console.log(`deu ruim`);
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues: {
      addressNickname: "",
      addressTypeResident: "",
      addressTypeStreet: "",
      addressStreet: "",
      addressNumber: "",
      addressNeighborhood: "",
      addressCity: "",
      addressState: "",
      addressCountry: "",
      addressZipCode: "",
      addressComments: "",
      addressResidentDefault: false,
      addressDeliveryDefault: false,
      addressBillingDefault: false,
    },
    validationSchema: addressSchema,
    onSubmit,
  });

  return (
    <Account>
      <div className="page-new-address">
        <style jsx>{styles}</style>
        <h1>Novo Endereço</h1>
        <form onSubmit={handleSubmit}>
          <article>
            <header>Dados Endereço</header>
            <div className="residential-address-inputs">
              <div className="row">
                <Input
                  label="Identificação do endereço (apelido)"
                  name="addressNickname"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressNickname ? errors.addressNickname : ""
                  }
                />
                <Select
                  label="Tipo Residência"
                  name="addressTypeResident"
                  options={residentOptions}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressTypeResident
                      ? errors.addressTypeResident
                      : ""
                  }
                />
              </div>
              <div className="row">
                <Select
                  label="Tipo Logradouro"
                  name="addressTypeStreet"
                  options={streetOptions}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressTypeStreet ? errors.addressTypeStreet : ""
                  }
                />
                <Input
                  label="Logradouro"
                  name="addressStreet"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressStreet ? errors.addressStreet : ""}
                />
                <Input
                  label="Número"
                  name="addressNumber"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressNumber ? errors.addressNumber : ""}
                />
              </div>
              <div className="row">
                <Input
                  label="Bairro"
                  name="addressNeighborhood"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressNeighborhood
                      ? errors.addressNeighborhood
                      : ""
                  }
                />
                <Input
                  label="Cidade"
                  name="addressCity"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressCity ? errors.addressCity : ""}
                />
                <Input
                  label="Estado"
                  name="addressState"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressState ? errors.addressState : ""}
                />
                <Input
                  label="País"
                  name="addressCountry"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressCountry ? errors.addressCountry : ""}
                />
              </div>
              <div className="row">
                <Input
                  label="CEP"
                  name="addressZipCode"
                  value={values.addressZipCode}
                  onChange={(e) => {
                    e.target.value = normalizeCepNumber(e.target.value);
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  errorMsg={touched.addressZipCode ? errors.addressZipCode : ""}
                />
                <Input
                  label="Observações"
                  name="addressComments"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressComments ? errors.addressComments : ""
                  }
                />
              </div>
              <div className="checkbox-container">
                <Checkbox
                  id="addressResidentDefault"
                  name="addressResidentDefault"
                  label="Configurar como endereço residencial padrão"
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <Checkbox
                  id="addressDeliveryDefault"
                  name="addressDeliveryDefault"
                  label="Configurar como endereço de entrega padrão"
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <Checkbox
                  id="addressBillingDefault"
                  name="addressBillingDefault"
                  label="Configurar como endereço de cobrança padrão"
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </div>
            </div>
          </article>
          <div className="container-button">
            <Button textButton="Registrar" disabled={isSubmitting} />
          </div>
        </form>
      </div>
    </Account>
  );
}
