import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import {
  deleteCustomerAddress,
  getCustomerAddress,
} from "../../../../api/customer";
import Button from "../../../../components/Button";
import dlv from "dlv";
import styles from "./styles";

export default function DeleteAddresses() {
  const router = useRouter();
  const [address, setAddress] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    (async function () {
      try {
        const addressBack = await getCustomerAddress(1, router.query.id);
        setAddress(addressBack.data);
      } catch (error) {
        console.log(`deu ruim`);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const handleDelete = async () => {
    try {
      await deleteCustomerAddress(1, router.query.id);
      router.back();
    } catch (error) {
      if (error.code === "ERR_BAD_REQUEST") setError(true);
      console.log(`deu ruim - delete`);
    }
  };

  if (loading) return <p>Loading</p>;

  if (error)
    return (
      <div className="delete-address">
        <style jsx>{styles}</style>
        <p>Não é possível excluir um endereço com confirações</p>
        <div className="container-buttons">
          <Button
            textButton="Voltar"
            className="secondary"
            onClick={() => router.back()}
          />
        </div>
      </div>
    );

  return (
    <div className="delete-address">
      <style jsx>{styles}</style>
      <p>Você realmente deseja excluir o {dlv(address, "addressNickname")}?</p>
      <div className="container-buttons">
        <Button
          textButton="Voltar"
          className="secondary"
          onClick={() => router.back()}
        />
        <Button
          textButton="Excluir"
          className="terciary"
          onClick={handleDelete}
        />
      </div>
    </div>
  );
}
