import React, { useEffect, useState } from "react";
import Button from "../../../../components/Button";
import Checkbox from "../../../../components/Checkbox";
import Input from "../../../../components/Input";
import { normalizeCepNumber } from "../../../../lib/mask";
import Select from "../../../../components/Select";
import { residentOptions, streetOptions } from "../../../../lib/selectOptions";
import { useFormik } from "formik";
import { addressSchema } from "../../../../schemas";
import { useRouter } from "next/router";
import { getCustomerAddress, updateAddress } from "../../../../api/customer";
import Account from "../..";
import styles from "./styles";
import dlv from "dlv";

export default function EditAddress() {
  const router = useRouter();
  const [address, setAddress] = useState({});

  const onSubmit = async (values, actions) => {
    try {
      const addressId = router.query.id;
      await updateAddress(1, addressId, values);
      router.back();
    } catch (error) {
      console.log(`deu ruim`);
    }
  };

  useEffect(() => {
    (async function () {
      try {
        const addressId = router.query.id;
        const addressBack = await getCustomerAddress(1, addressId);
        setAddress(addressBack.data);
      } catch (error) {
        console.log(`deu ruim`);
      }
    })();
  }, []);

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues: {
      addressNickname: dlv(address, "addressNickname"),
      addressTypeResident: dlv(address, "addressTypeResident"),
      addressTypeStreet: dlv(address, "addressTypeStreet"),
      addressStreet: dlv(address, "addressStreet"),
      addressNumber: dlv(address, "addressNumber"),
      addressNeighborhood: dlv(address, "addressNeighborhood"),
      addressCity: dlv(address, "addressCity"),
      addressState: dlv(address, "addressState"),
      addressCountry: dlv(address, "addressCountry"),
      addressZipCode: dlv(address, "addressZipCode"),
      addressComments: dlv(address, "addressComments"),
      addressResidentDefault: dlv(address, "addressResidentDefault"),
      addressDeliveryDefault: dlv(address, "addressDeliveryDefault"),
      addressBillingDefault: dlv(address, "addressBillingDefault"),
    },
    validationSchema: addressSchema,
    enableReinitialize: true,
    onSubmit,
  });

  return (
    <Account>
      <div className="page-edit-address">
        <style jsx>{styles}</style>
        <h1>Novo Endereço</h1>
        <form onSubmit={handleSubmit}>
          <article>
            <header>Endereço Residencial</header>
            <div className="residential-address-inputs">
              <div className="row">
                <Input
                  label="Identificação do endereço (apelido)"
                  value={values.addressNickname}
                  name="addressNickname"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressNickname ? errors.addressNickname : ""
                  }
                />
                <Select
                  value={values.addressTypeResident}
                  label="Tipo Residência"
                  name="addressTypeResident"
                  options={residentOptions}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressTypeResident
                      ? errors.addressTypeResident
                      : ""
                  }
                />
              </div>
              <div className="row">
                <Select
                  value={values.addressTypeStreet}
                  label="Tipo Logradouro"
                  name="addressTypeStreet"
                  options={streetOptions}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressTypeStreet ? errors.addressTypeStreet : ""
                  }
                />
                <Input
                  value={values.addressStreet}
                  label="Logradouro"
                  name="addressStreet"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressStreet ? errors.addressStreet : ""}
                />
                <Input
                  value={values.addressNumber}
                  label="Número"
                  name="addressNumber"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressNumber ? errors.addressNumber : ""}
                />
              </div>
              <div className="row">
                <Input
                  value={values.addressNeighborhood}
                  label="Bairro"
                  name="addressNeighborhood"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressNeighborhood
                      ? errors.addressNeighborhood
                      : ""
                  }
                />
                <Input
                  value={values.addressCity}
                  label="Cidade"
                  name="addressCity"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressCity ? errors.addressCity : ""}
                />
                <Input
                  value={values.addressState}
                  label="Estado"
                  name="addressState"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressState ? errors.addressState : ""}
                />
                <Input
                  value={values.addressCountry}
                  label="País"
                  name="addressCountry"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={touched.addressCountry ? errors.addressCountry : ""}
                />
              </div>
              <div className="row">
                <Input
                  label="CEP"
                  name="addressZipCode"
                  value={values.addressZipCode}
                  onChange={(e) => {
                    e.target.value = normalizeCepNumber(e.target.value);
                    handleChange(e);
                  }}
                  onBlur={handleBlur}
                  errorMsg={touched.addressZipCode ? errors.addressZipCode : ""}
                />
                <Input
                  value={values.addressComments}
                  label="Observações"
                  name="addressComments"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errorMsg={
                    touched.addressComments ? errors.addressComments : ""
                  }
                />
              </div>
              <div className="checkbox-container">
                {dlv(address, "addressResidentDefault") ? (
                  <Checkbox
                    id="addressResidentDefault"
                    name="addressResidentDefault"
                    label="Configurar como endereço residencial padrão"
                    disabled={true}
                    checked={true}
                  />
                ) : (
                  <Checkbox
                    id="addressResidentDefault"
                    name="addressResidentDefault"
                    label="Configurar como endereço residencial padrão"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                )}
                {dlv(address, "addressDeliveryDefault") ? (
                  <Checkbox
                    id="addressDeliveryDefault"
                    name="addressDeliveryDefault"
                    label="Configurar como endereço de entrega padrão"
                    disabled={true}
                    checked={true}
                  />
                ) : (
                  <Checkbox
                    id="addressDeliveryDefault"
                    name="addressDeliveryDefault"
                    label="Configurar como endereço de entrega padrão"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                )}
                {dlv(address, "addressBillingDefault") ? (
                  <Checkbox
                    id="addressBillingDefault"
                    name="addressBillingDefault"
                    label="Configurar como endereço de cobrança padrão"
                    disabled={true}
                    checked={true}
                  />
                ) : (
                  <Checkbox
                    id="addressBillingDefault"
                    name="addressBillingDefault"
                    label="Configurar como endereço de cobrança padrão"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                )}
              </div>
            </div>
          </article>
          <div className="container-button">
            <Button
              textButton="Voltar"
              type="button"
              className="secondary"
              onClick={() => router.back()}
            />
            <Button textButton="Editar" disabled={isSubmitting} />
          </div>
        </form>
      </div>
    </Account>
  );
}
