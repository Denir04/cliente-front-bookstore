import css from "styled-jsx/css";

export default css`
  .success-container {
    margin-top: 200px;
  }

  .page-edit-address form {
    border: 1px solid var(--color-zero);
    padding: 18px;
    background-color: var(--color-secondary);
  }

  .page-edit-address h1 {
    margin-bottom: 20px;
  }

  .page-edit-address form article {
    margin-bottom: 15px;
  }

  .page-edit-address form article header {
    font-size: 1.2rem;
    font-weight: bold;
    margin-bottom: 20px;
    border-bottom: 2px solid var(--color-zero);
  }

  .page-edit-address
    form
    article
    .residential-address-inputs
    .row:nth-child(1) {
    display: grid;
    grid-template-columns: 2fr 1fr;
    gap: 20px;
  }

  .page-edit-address
    form
    article
    .residential-address-inputs
    .row:nth-child(2) {
    display: grid;
    grid-template-columns: 1fr 3fr 1fr;
    gap: 20px;
  }

  .page-edit-address
    form
    article
    .residential-address-inputs
    .row:nth-child(3) {
    display: grid;
    grid-template-columns: 3fr 3fr 1fr 2fr;
    gap: 20px;
  }

  .page-edit-address
    form
    article
    .residential-address-inputs
    .row:nth-child(4) {
    display: grid;
    grid-template-columns: 1fr 3fr;
    gap: 20px;
  }

  .page-edit-address .checkbox-container {
    margin-top: 10px;
  }

  .page-edit-address .container-button {
    display: flex;
    gap: 10px;
  }
`;
