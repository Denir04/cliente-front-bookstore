import css from "styled-jsx/css";

export default css`
  .options-n-details {
    display: flex;
    gap: 35px;
  }

  .options-n-details .options {
    border: 1px solid var(--color-zero);
    width: 20%;
    height: fit-content;
    background-color: var(--color-secondary);
    padding: 15px 20px;
    display: flex;
    flex-direction: column;
    gap: 5px;
    margin-top: 50px;
  }

  .options-n-details .options span {
    color: var(--color-zero);
    font-size: 1.1rem;
  }

  .options-n-details .details {
    width: 70%;
  }
`;
