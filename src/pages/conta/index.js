import React from "react";
import styles from "./styles";
import Link from "next/link";

export default function Account({ children }) {
  return (
    <div className="page-account">
      <style jsx>{styles}</style>
      <main className="options-n-details">
        <section className="options">
          <Link href="/conta/minha-conta">
            <span>Minha Conta</span>
          </Link>
          <Link href="/conta/meus-enderecos">
            <span>Meus Endereços</span>
          </Link>
          <Link href="/conta/meus-cartoes">
            <span>Meus Cartões</span>
          </Link>
          <Link href="/conta/meus-pedidos">
            <span>Meus Pedidos</span>
          </Link>
          <Link href="/conta/minhas-trocas">
            <span>Minhas Trocas</span>
          </Link>
          <Link href="/conta/meus-cupons">
            <span>Meus Cupons</span>
          </Link>
        </section>
        <section className="details">{children}</section>
      </main>
    </div>
  );
}
