import React from "react";
import { useRouter } from "next/router";
import Image from "next/image";

import styles from "./styles";
import GoBack from "../../../../components/GoBack";
import img1984 from "../../../../static/1984.jpg";

export default function ViewExchange() {
  const router = useRouter();

  return (
    <div className="view-request">
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <h1>N° Pedido Troca #5437587843535</h1>
      <article className="request-card-full">
        <header>
          <p>Data do Pedido: 10/08/2023</p>
          <p className="status">Em Troca</p>
        </header>
        <section className="details">
          <article className="selected-product-card">
            <div className="column">
              <div className="image-container">
                <Image src={img1984} alt={"1984"} fill priority />
              </div>
              <div className="product-info">
                <h3>1984</h3>
                <p className="amount">Quantidade: 2</p>
              </div>
            </div>
            <p className="price">R$ 30,00</p>
          </article>
        </section>
        <footer>
          <p>Valor Final</p>
          <p>R$ 60,00</p>
        </footer>
      </article>
    </div>
  );
}
