import css from "styled-jsx/css";

export default css`
  .view-request h1 {
    margin-bottom: 20px;
  }

  .request-card-full {
    border: 1px solid var(--color-zero);
    background-color: var(--color-secondary);
    margin-bottom: 30px;
  }

  .request-card-full header {
    border-bottom: 1px solid var(--color-zero);
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 20px;
    height: 35px;
  }

  .request-card-full header .status {
    font-size: 1.1rem;
    font-weight: bold;
  }

  .request-card-full .details {
    padding: 15px 20px 10px;
  }

  .request-card-full .details article {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 5px;
  }

  .request-card-full .details article .image-container {
    position: relative;
    height: 120px;
    width: 90px;
  }

  .request-card-full .details article .column {
    display: flex;
    align-items: center;
    gap: 10px;
  }

  .request-card-full footer {
    display: flex;
    padding: 15px 20px;
    justify-content: space-between;
    border-top: 1px solid var(--color-zero);
  }
`;
