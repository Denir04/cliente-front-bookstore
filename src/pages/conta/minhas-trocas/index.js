import React from "react";

import Account from "..";
import RequestCard from "../../../components/RequestCard";

import styles from "./styles";
import { useRouter } from "next/router";

export default function MyExchanges() {
  const router = useRouter();

  return (
    <Account>
      <div className="page-my-requests">
        <style jsx>{styles}</style>
        <h1>Minhas Trocas</h1>
        <section className="requests">
          <RequestCard
            id="5437587843535"
            status="Em Troca"
            requestDate="10/08/2023"
            totalValue="60,00"
            onView={() => router.push("/conta/minhas-trocas/em-troca")}
          />
          <RequestCard
            id="6712389089323"
            status="Troca Autorizada"
            requestDate="04/08/2023"
            totalValue="25,00"
            onView={() => router.push("/conta/minhas-trocas/em-troca")}
          />
        </section>
      </div>
    </Account>
  );
}
