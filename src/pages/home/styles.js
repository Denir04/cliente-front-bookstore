import css from "styled-jsx/css";

export default css`
  .home-page h1 {
    margin-right: 150px;
    width: 300px;
  }

  .home-page .title-n-search-text {
    display: flex;
    gap: 20px;
    height: 40px;
    margin-bottom: 30px;
  }

  .title-n-search-text .form-search-text {
    padding: 10px 15px;
    width: 100%;
    height: fit-content;
    display: grid;
    gap: 15px;
    grid-template-columns: 5fr 1fr;
    align-items: center;
    margin-right: 50px;
    border: 2px solid var(--color-zero);
    background-color: var(--color-secondary);
  }

  .filter-n-products {
    display: flex;
    justify-content: space-between;
  }

  .filter-container {
    width: 300px;
    border: 2px solid var(--color-zero);
    padding: 20px;
    background-color: var(--color-secondary);
    height: fit-content;
  }

  .filter-container .filter-actions {
    display: flex;
    gap: 10px;
  }

  .filter-container h2 {
    font-size: 1.2rem;
    font-weight: bold;
    margin-bottom: 10px;
  }

  .filter-container h3 {
    font-weight: bold;
    margin-bottom: 5px;
  }

  .filter-container article {
    margin-bottom: 15px;
  }

  .products-container {
    width: fit-content;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    column-gap: 20px;
    row-gap: 30px;
    margin-right: 50px;
    height: fit-content;
  }
`;
