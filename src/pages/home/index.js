import React, { useEffect, useState } from "react";
import ProductCard from "../../components/ProductCard";
import styles from "./styles";
import Checkbox from "../../components/Checkbox";
import IntervarInput from "../../components/IntervalInput";
import Button from "../../components/Button";
import Input from "../../components/Input";
import { getAllProducts } from "../../api/products";

export default function Home() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    (async function () {
      try {
        const productsBack = await getAllProducts();
        console.log(productsBack.data);
        setProducts(productsBack.data);
      } catch (error) {
        console.log("deu ruim - getAllProducts");
      }
    })();
  }, []);

  return (
    <div className="home-page">
      <style jsx>{styles}</style>
      <div className="title-n-search-text">
        <h1>Home</h1>
        <form onSubmit={(e) => e.preventDefault()} className="form-search-text">
          <Input placeholder="Buscar produto" />
          <Button textButton="Buscar" />
        </form>
      </div>
      <div className="filter-n-products">
        <section className="filter-container">
          <h2>Filtros</h2>
          <article className="filter-actions">
            <Button textButton={"Aplicar Filtros"} className="primary" />
            <Button textButton={"Limpar Filtros"} className="secondary" />
          </article>
          <article>
            <h3>Categoria</h3>
            <Checkbox label="Fantasia" id="Fantasia" />
            <Checkbox label="Aventura" id="Aventura" />
            <Checkbox label="Romance" id="Romance" />
            <Checkbox label="Ficção Científica" id="Ficção Científica" />
            <Checkbox label="Biografia" id="Biografia" />
          </article>
          <article>
            <h3>Editora</h3>
            <Checkbox label="Companhia da Letras" id="Companhia da Letras" />
            <Checkbox label="Aleph" id="Aleph" />
            <Checkbox label="Suma" id="Suma" />
            <Checkbox
              label="Grupo Editorial Record"
              id="Grupo Editorial Record"
            />
          </article>
          <article>
            <h3>Ano Lançamento</h3>
            <IntervarInput />
          </article>
          <article>
            <h3>Número de Páginas</h3>
            <IntervarInput />
          </article>
        </section>
        <section className="products-container">
          {products.map((product) => (
            <ProductCard
              key={product.productId}
              id={product.productId}
              title={product.productTitle}
              price={product.productSale}
              imageUrl={product.productImageUrl}
            />
          ))}
        </section>
      </div>
    </div>
  );
}
