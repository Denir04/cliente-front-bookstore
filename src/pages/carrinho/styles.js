import css from "styled-jsx/css";

export default css`
  .page-shop-cart h1 {
    margin-bottom: 20px;
  }

  .products-n-details {
    display: flex;
    gap: 35px;
  }

  .products-n-details .products-items {
    padding: 15px;
    border: 1px solid var(--color-zero);
    width: 65%;
    background-color: var(--color-secondary);
  }

  .products-n-details .products-items h2,
  .products-n-details .order-details h2 {
    font-weight: bold;
    font-size: 1.2rem;
    margin-bottom: 30px;
    padding: 8px 0;
    border-bottom: 2px solid var(--color-zero);
  }

  .products-n-details .order-details {
    padding: 15px 25px;
    border: 1px solid var(--color-zero);
    width: 35%;
    background-color: var(--color-secondary);
    height: fit-content;
  }

  .products-n-details .order-details .detail {
    display: flex;
    justify-content: space-between;
    margin-bottom: 50px;
  }
`;
