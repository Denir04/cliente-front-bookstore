import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import GoBack from "../../components/GoBack";
import ProductShopCard from "../../components/ProductShopCard";
import Button from "../../components/Button";

import styles from "./styles";
import { getShopCart, removeShopCart } from "../../api/products";
import { startOrder } from "../../api/order";

export default function ShoppingCart() {
  const router = useRouter();
  const [productItems, setProductItems] = useState([]);
  const [totalPrice, setTotalPrice] = useState();
  const [removed, setRemoved] = useState(true);
  const [changedCounter, setChangedCounter] = useState(true);

  useEffect(() => {
    (async function () {
      try {
        const shopCartBack = await getShopCart();
        setProductItems(shopCartBack.data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, [removed, changedCounter]);

  useEffect(() => {
    let total = 0;
    productItems.forEach(({ itemProduct, itemProductQuantity }) => {
      total += itemProduct.productSale * itemProductQuantity;
    });
    setTotalPrice(total);
  }, [productItems]);

  const handleRemoveItem = async (itemId) => {
    try {
      console.log(itemId);
      await removeShopCart(itemId);
      setRemoved(!removed);
    } catch (error) {
      console.log(error);
    }
  };

  const handleStartOrder = async () => {
    try {
      await startOrder();
      router.push(`/carrinho/endereco-entrega`);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="page-shop-cart">
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <h1>Meu Carrinho</h1>
      <main className="products-n-details">
        <section className="products-items">
          <h2>Produtos</h2>
          {productItems.map(({ itemProduct, itemProductQuantity }) => (
            <ProductShopCard
              key={itemProduct.productId}
              productId={itemProduct.productId}
              imgUrl={itemProduct.productImageUrl}
              title={itemProduct.productTitle}
              price={itemProduct.productSale}
              quantity={itemProductQuantity}
              limit={itemProduct.productQuantity}
              onRemove={handleRemoveItem}
              changedCounter={changedCounter}
              onChangedCounter={setChangedCounter}
            />
          ))}
        </section>
        <section className="order-details">
          <h2>Detalhes do Pedido</h2>
          <article className="detail">
            <p>Produto(s)</p>
            <p>R$ {totalPrice?.toFixed(2)}</p>
          </article>
          <Button textButton="Iniciar Pedido" onClick={handleStartOrder} />
        </section>
      </main>
    </div>
  );
}
