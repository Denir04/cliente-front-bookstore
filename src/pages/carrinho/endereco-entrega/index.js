import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import styles from "./styles";
import Button from "../../../components/Button";
import GoBack from "../../../components/GoBack";
import SelectAddressCard from "../../../components/SelectAddressCard";
import { getCustomerAddresses } from "../../../api/customer";
import { getSummary, sendAddressDelivery } from "../../../api/order";

export default function SelectDeliveryAddress() {
  const [addresses, setAddresses] = useState([]);
  const [orderDetail, setOrderDetail] = useState({});
  const router = useRouter();

  useEffect(() => {
    (async function () {
      try {
        const addressBack = await getCustomerAddresses(1);
        setAddresses(addressBack.data);
      } catch (error) {
        console.log(error);
      }
    })();
    (async function () {
      try {
        const orderDetailBack = await getSummary();
        console.log(orderDetailBack.data);
        setOrderDetail(orderDetailBack.data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  const handleChangeRadio = async (event) => {
    const selectedAddressId = event.target.value;
    try {
      const shippingBack = await sendAddressDelivery(1, selectedAddressId);
      setOrderDetail(shippingBack.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="page-select-address">
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <h1>Endereço de Entrega</h1>
      <main className="addresses-n-details">
        <section className="addresses">
          {addresses?.map((address) => (
            <SelectAddressCard
              key={address.addressId}
              addressId={address.addressId}
              typeStreet={address.addressTypeStreet}
              nickname={address.addressNickname}
              neighborhood={address.addressNickname}
              street={address.addressStreet}
              number={address.addressNumber}
              onChange={handleChangeRadio}
            />
          ))}
          <p
            onClick={() =>
              router.push(`/carrinho/endereco-entrega/novo-endereco`)
            }
          >
            Usar um novo endereço
          </p>
        </section>
        <section className="order-details">
          <h2>Detalhes do Pedido</h2>
          <article className="detail">
            <p>Produto(s)</p>
            <p>R$ {orderDetail.productsAmount?.toFixed(2)}</p>
          </article>
          {orderDetail["shippingValue"] && (
            <article className="detail">
              <p>Frete</p>
              <p>R$ {orderDetail["shippingValue"]?.toFixed(2)}</p>
            </article>
          )}
          <article className="detail sub-total">
            <p>Sub-Total</p>
            <p>R$ {orderDetail.subtotal?.toFixed(2)}</p>
          </article>
          <Button
            textButton="Continuar Pedido"
            onClick={() => router.push("/carrinho/forma-pagamento")}
          />
        </section>
      </main>
    </div>
  );
}
