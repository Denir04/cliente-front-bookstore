import css from "styled-jsx/css";

export default css`
  .page-select-address h1 {
    margin-bottom: 20px;
  }

  .addresses-n-details {
    display: flex;
    gap: 35px;
  }

  .addresses-n-details .addresses {
    padding: 15px;
    border: 1px solid var(--color-zero);
    width: 65%;
    background-color: var(--color-secondary);
  }

  .addresses-n-details .addresses p {
    margin-top: 50px;
    text-align: center;
    cursor: pointer;
    text-decoration: underline;
  }

  .addresses-n-details .order-details h2 {
    font-weight: bold;
    font-size: 1.2rem;
    margin-bottom: 30px;
    padding: 8px 0;
    border-bottom: 2px solid var(--color-zero);
  }

  .addresses-n-details .order-details {
    padding: 15px 25px;
    border: 1px solid var(--color-zero);
    width: 35%;
    background-color: var(--color-secondary);
    height: fit-content;
  }

  .addresses-n-details .order-details .detail {
    display: flex;
    justify-content: space-between;
    margin-bottom: 5px;
  }

  .addresses-n-details .order-details .detail.sub-total {
    border-top: 2px solid var(--color-zero);
    padding: 8px 0;
    margin-bottom: 20px;
    margin-top: 10px;
  }
`;
