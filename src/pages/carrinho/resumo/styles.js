import css from "styled-jsx/css";

export default css`
  .page-summary-buying h1 {
    margin-bottom: 20px;
  }

  .summary-n-details {
    display: flex;
    gap: 35px;
  }

  .summary-n-details .summary {
    padding: 15px;
    border: 1px solid var(--color-zero);
    width: 65%;
    background-color: var(--color-secondary);
  }

  .summary-n-details .summary h2 {
    font-size: 1.3rem;
    font-weight: bold;
    border-bottom: 2px solid var(--color-zero);
    margin-bottom: 20px;
  }

  .summary-n-details .summary .products {
    display: flex;
    flex-direction: column;
    gap: 10px;
    margin-bottom: 20px;
  }

  .summary-n-details .order-details h2 {
    font-weight: bold;
    font-size: 1.2rem;
    margin-bottom: 30px;
    padding: 8px 0;
    border-bottom: 2px solid var(--color-zero);
  }

  .summary-n-details .order-details {
    padding: 15px 25px;
    border: 1px solid var(--color-zero);
    width: 35%;
    background-color: var(--color-secondary);
    height: fit-content;
  }

  .summary-n-details .order-details .detail {
    display: flex;
    justify-content: space-between;
    margin-bottom: 5px;
  }

  .summary-n-details .order-details .detail.sub-total {
    border-top: 2px solid var(--color-zero);
    padding: 8px 0;
    margin-bottom: 20px;
    margin-top: 10px;
  }
`;
