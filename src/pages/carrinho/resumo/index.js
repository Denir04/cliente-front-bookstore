import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import GoBack from "../../../components/GoBack";
import Button from "../../../components/Button";

import styles from "./styles";
import SelectedProductCard from "../../../components/SelectedProductCard";
import SelectAddressCard from "../../../components/SelectAddressCard";
import imgLittlePrince from "../../../static/pequeno-principe.jpg";
import img1984 from "../../../static/1984.jpg";
import SelectCreditCard from "../../../components/SelectCreditCard";
import { finishOrder, getSummary } from "../../../api/order";

export default function Summary() {
  const router = useRouter();
  const [orderDetail, setOrderDetail] = useState({});

  useEffect(() => {
    (async function () {
      try {
        const orderDetailBack = await getSummary();
        setOrderDetail(orderDetailBack.data);
        console.log(orderDetailBack.data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  const handleFinishOrder = async () => {
    try {
      await finishOrder();
      router.push(`/conta/meus-pedidos`);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="page-summary-buying">
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <h1>Resumo do Pedido</h1>
      <main className="summary-n-details">
        <section className="summary">
          <h2>Produtos</h2>
          <div className="products">
            {orderDetail["products"]?.map((product, index) => (
              <SelectedProductCard
                key={index}
                imgUrl={product["productImage"]}
                price={product["productPrice"]?.toFixed(2)}
                amount={product["productQuantity"]}
              />
            ))}
          </div>
          <h2>Endereço de entrega</h2>
          <SelectAddressCard
            nickname={orderDetail["billingAddress"]?.nickname}
            neighborhood={orderDetail["billingAddress"]?.neighborhood}
            typeStreet={orderDetail["billingAddress"]?.typeStreet}
            street={orderDetail["billingAddress"]?.street}
            number={orderDetail["billingAddress"]?.number}
            selected
          />
          <h2>Endereço de cobrança</h2>
          <SelectAddressCard
            nickname={orderDetail["shippingAddress"]?.nickname}
            neighborhood={orderDetail["shippingAddress"]?.neighborhood}
            typeStreet={orderDetail["shippingAddress"]?.typeStreet}
            street={orderDetail["shippingAddress"]?.street}
            number={orderDetail["shippingAddress"]?.number}
            selected
          />
          <h2>Forma de Pagamento</h2>
          {orderDetail["paymentCards"]?.map((card, index) => (
            <SelectCreditCard
              key={index}
              selected
              brand="Visa"
              lastNumbers="2967"
              name="visa-2967"
              creditValue={card["paymentCardPrice"].toFixed(2)}
            />
          ))}
        </section>
        <section className="order-details">
          <h2>Detalhes do Pedido</h2>
          <article className="detail">
            <p>Produto(s)</p>
            <p>R$ {orderDetail.productsAmount?.toFixed(2)}</p>
          </article>
          {orderDetail["shippingValue"] && (
            <article className="detail">
              <p>Frete</p>
              <p>R$ {orderDetail["shippingValue"]?.toFixed(2)}</p>
            </article>
          )}
          <article className="detail sub-total">
            <p>Sub-Total</p>
            <p>R$ {orderDetail.subtotal?.toFixed(2)}</p>
          </article>
          <Button textButton="Finalizar Pedido" onClick={handleFinishOrder} />
        </section>
      </main>
    </div>
  );
}
