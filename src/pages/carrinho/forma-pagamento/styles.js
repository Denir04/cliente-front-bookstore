import css from "styled-jsx/css";

export default css`
  .page-select-payment h1 {
    margin-bottom: 20px;
  }

  .cards-n-details {
    display: flex;
    gap: 35px;
  }

  .cards-n-details .cards {
    padding: 15px;
    border: 1px solid var(--color-zero);
    width: 65%;
    background-color: var(--color-secondary);
    display: flex;
    flex-direction: column;
    gap: 10px;
    height: fit-content;
  }

  .cards-n-details .cards p {
    margin-top: 50px;
    text-align: center;
    cursor: pointer;
    text-decoration: underline;
  }

  .cards-n-details .order-details h2 {
    font-weight: bold;
    font-size: 1.2rem;
    margin-bottom: 30px;
    padding: 8px 0;
    border-bottom: 2px solid var(--color-zero);
  }

  .cards-n-details .order-details {
    padding: 15px 25px;
    border: 1px solid var(--color-zero);
    width: 35%;
    background-color: var(--color-secondary);
    height: fit-content;
  }

  .cards-n-details .order-details .apply-ticket {
    width: 100%;
    display: flex;
    justify-content: space-between;
    gap: 5px;
    margin-bottom: 25px;
  }

  .cards-n-details .order-details .apply-ticket input {
    width: 70%;
  }

  .cards-n-details .order-details .apply-ticket .button-container {
    width: 30%;
  }

  .cards-n-details .order-details .detail {
    display: flex;
    justify-content: space-between;
    margin-bottom: 5px;
  }

  .cards-n-details .order-details .detail p {
    display: flex;
    align-items: center;
  }

  .cards-n-details .order-details .detail p .remove-ticket {
    font-size: 0.7rem;
    margin-left: 8px;
    cursor: pointer;
    color: var(--color-terciary);
  }

  .cards-n-details .order-details .detail.sub-total {
    border-top: 2px solid var(--color-zero);
    padding: 8px 0;
    margin-bottom: 20px;
    margin-top: 10px;
  }
`;
