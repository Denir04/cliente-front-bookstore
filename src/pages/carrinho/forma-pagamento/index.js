import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import GoBack from "../../../components/GoBack";
import Button from "../../../components/Button";

import styles from "./styles";
import SelectCreditCard from "../../../components/SelectCreditCard";
import { getSummary, sendPaymentWay } from "../../../api/order";
import { getCreditCards } from "../../../api/customer";

export default function SelectPaymentMethod() {
  const [creditCards, setCreditCards] = useState([]);
  const [orderDetail, setOrderDetail] = useState({});
  const [selectedCards, setSelectedCards] = useState(new Map());
  const router = useRouter();

  useEffect(() => {
    (async function () {
      try {
        const creditCardBack = await getCreditCards(1);
        setCreditCards(creditCardBack.data);
        console.log(creditCardBack.data);
      } catch (error) {
        console.log(error);
      }
    })();
    (async function () {
      try {
        const orderDetailBack = await getSummary();
        console.log(orderDetailBack.data);
        setOrderDetail(orderDetailBack.data);
      } catch (error) {
        console.log(error);
      }
    })();
  }, []);

  const handleSubmit = async () => {
    try {
      const creditCardChosen = new Array();
      selectedCards.forEach((price, cardId) => {
        const findedCard = creditCards.find(
          (card) => card["cardId"] === Number(cardId)
        );
        if (findedCard)
          creditCardChosen.push({
            cardId: findedCard["cardId"],
            brand: findedCard["cardBrand"],
            number: findedCard["cardNumber"],
            name: findedCard["cardName"],
            price: Number(price),
            parcel: 1,
          });
      });
      await sendPaymentWay(creditCardChosen);
      router.push(`/carrinho/resumo`);
    } catch (error) {
      console.log(error);
    }
  };

  const handleChangeBox = async (cardId) => {
    if (selectedCards.has(cardId)) {
      const newMap = new Map(selectedCards);
      newMap.delete(cardId);
      setSelectedCards(newMap);
    } else {
      setSelectedCards(new Map(selectedCards.set(cardId, 0)));
    }
  };

  const handleChangeCardValue = async (cardId, value) => {
    setSelectedCards(new Map(selectedCards.set(cardId, value)));
    console.log(selectedCards);
  };

  return (
    <div className="page-select-payment">
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <h1>Forma de Pagamento</h1>
      <main className="cards-n-details">
        <section className="cards">
          {creditCards.map((card) => {
            const lastNumbers = card[`cardNumber`]?.substr(
              card[`cardNumber`].length - 4
            );
            return (
              <SelectCreditCard
                key={card["cardId"]}
                id={card["cardId"]}
                brand={card["cardBrand"]}
                lastNumbers={lastNumbers}
                name={`${card["cardBrand"]}-${lastNumbers}`}
                onChangeBox={handleChangeBox}
                onChangeValue={handleChangeCardValue}
              />
            );
          })}
          <p onClick={() => router.push(`forma-pagamento/novo-cartao`)}>
            Usar um novo cartão de crédito
          </p>
        </section>
        <section className="order-details">
          <h2>Detalhes do Pedido</h2>
          <div className="apply-ticket">
            <input type="text" />
            <div className="button-container">
              <Button textButton="Aplicar" />
            </div>
          </div>
          <article className="detail">
            <p>Produto(s)</p>
            <p>R$ {orderDetail[`productsAmount`]?.toFixed(2)}</p>
          </article>
          {orderDetail["shippingValue"] && (
            <article className="detail">
              <p>Frete</p>
              <p>R$ {orderDetail["shippingValue"]?.toFixed(2)}</p>
            </article>
          )}
          {/*<article className="detail">
            <p>
              Cupom <span className="remove-ticket">Remover</span>
            </p>
            <p>- R$ 10,00</p>
          </article>*/}
          <article className="detail sub-total">
            <p>Total a Pagar</p>
            <p>R$ {orderDetail[`subtotal`]?.toFixed(2)}</p>
          </article>
          <Button
            textButton="Continuar Pedido"
            onClick={() => handleSubmit()}
          />
        </section>
      </main>
    </div>
  );
}
