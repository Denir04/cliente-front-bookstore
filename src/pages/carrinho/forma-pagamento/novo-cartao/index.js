import { useRouter } from "next/router";
import React from "react";
import { createCreditCard } from "../../../../api/customer";
import { useFormik } from "formik";
import { creditCardSchema } from "../../../../schemas";
import styles from "./styles";
import Input from "../../../../components/Input";
import {
  normalizeCodSeguranca,
  normalizeCreditCardNumber,
} from "../../../../lib/mask";
import Select from "../../../../components/Select";
import { brandOptions } from "../../../../lib/selectOptions";
import Button from "../../../../components/Button";

export default function NewCreditCard() {
  const router = useRouter();

  const onSubmit = async (values, actions) => {
    try {
      await createCreditCard(1, values);
      router.back();
    } catch (error) {
      console.log(`deu ruim`);
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues: {
      cardName: "",
      cardNumber: "",
      cardBrand: "",
      cardSecurityCode: "",
    },
    validationSchema: creditCardSchema,
    onSubmit,
  });

  return (
    <div className="page-new-credit-card">
      <style jsx>{styles}</style>
      <h1>Novo Cartão de Crédito</h1>
      <form onSubmit={handleSubmit}>
        <article>
          <header>Dados Cartão de Crédito</header>
          <div className="credit-card-inputs">
            <Input
              label="Nome Impresso"
              name="cardName"
              onChange={handleChange}
              onBlur={handleBlur}
              errorMsg={touched.cardName ? errors.cardName : ""}
            />
            <Input
              label="Número Impresso"
              name="cardNumber"
              onChange={(e) => {
                e.target.value = normalizeCreditCardNumber(e.target.value);
                handleChange(e);
              }}
              onBlur={handleBlur}
              errorMsg={touched.cardNumber ? errors.cardNumber : ""}
            />
            <div className="row">
              <Select
                label="Bandeira"
                name="cardBrand"
                options={brandOptions}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.cardBrand ? errors.cardBrand : ""}
              />
              <Input
                label="Código de Segurança"
                name="cardSecurityCode"
                onChange={(e) => {
                  e.target.value = normalizeCodSeguranca(e.target.value);
                  handleChange(e);
                }}
                onBlur={handleBlur}
                errorMsg={
                  touched.cardSecurityCode ? errors.cardSecurityCode : ""
                }
              />
            </div>
          </div>
        </article>
        <div className="container-button">
          <Button
            textButton="Voltar"
            className="secondary"
            type="button"
            onClick={() => router.back()}
          />
          <Button textButton="Registrar" disabled={isSubmitting} />
        </div>
      </form>
    </div>
  );
}
