import React, { useEffect, useState } from "react";
import styles from "./styles";
import { getActivedCustomers } from "../../api/customer";

export default function ActivedCustomers() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    (async function () {
      try {
        const customers = await getActivedCustomers();
        setCustomers(customers.data);
      } catch (error) {
        console.log(`deu ruim`);
      }
    })();
  }, []);

  return (
    <div className="page-actived-customers">
      <style jsx>{styles}</style>
      <h1>Clientes Ativos</h1>
      <ul>
        {customers.map((customer) => (
          <li key={customer[1]}>{customer[0]}</li>
        ))}
      </ul>
    </div>
  );
}
