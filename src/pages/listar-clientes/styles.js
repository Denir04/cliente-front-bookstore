import css from "styled-jsx/css";

export default css`
  .page-actived-customers h1 {
    margin-bottom: 20px;
  }

  .page-actived-customers ul {
    list-style: inside;
  }

  .page-actived-customers ul li {
    margin-bottom: 10px;
  }
`;
