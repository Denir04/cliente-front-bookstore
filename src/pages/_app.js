import "../styles/settings/colors.css";
import "../styles/generic/reset.css";
import "../styles/elements/base.css";

import Header from "../components/Header";
import Layout from "../components/Layout";

export default function MyApp({ Component, pageProps }) {
  return (
    <>
      <Header />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  );
}
