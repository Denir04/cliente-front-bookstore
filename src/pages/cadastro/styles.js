import css from "styled-jsx/css";

export default css`
  .success-container {
    margin-top: 200px;
  }

  .page-register-client form {
    border: 1px solid var(--color-zero);
    padding: 18px;
    background-color: var(--color-secondary);
  }

  .page-register-client h1 {
    margin-bottom: 20px;
  }

  .page-register-client form article {
    margin-bottom: 15px;
  }

  .page-register-client form article header {
    font-size: 1.2rem;
    font-weight: bold;
    margin-bottom: 20px;
    border-bottom: 2px solid var(--color-zero);
  }

  .page-register-client form article .basic-data-inputs,
  .page-register-client form article .residential-address-inputs,
  .page-register-client form article .login-data-inptus {
    display: flex;
    flex-direction: column;
    gap: 10px;
  }

  .page-register-client form article .basic-data-inputs .row:nth-child(2),
  .page-register-client form article .basic-data-inputs .row:nth-child(3) {
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 20px;
  }

  .page-register-client
    form
    article
    .residential-address-inputs
    .row:nth-child(1) {
    display: grid;
    grid-template-columns: 2fr 1fr;
    gap: 20px;
  }

  .page-register-client
    form
    article
    .residential-address-inputs
    .row:nth-child(2) {
    display: grid;
    grid-template-columns: 1fr 3fr 1fr;
    gap: 20px;
  }

  .page-register-client
    form
    article
    .residential-address-inputs
    .row:nth-child(3) {
    display: grid;
    grid-template-columns: 3fr 3fr 1fr 2fr;
    gap: 20px;
  }

  .page-register-client
    form
    article
    .residential-address-inputs
    .row:nth-child(4) {
    display: grid;
    grid-template-columns: 1fr 3fr;
    gap: 20px;
  }

  .page-register-client form article .login-data-inptus {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    gap: 20px;
  }

  .container-button {
    margin-top: 50px;
    width: 32%;
  }
`;
