import React, { useEffect, useState } from "react";
import Input from "../../components/Input";
import Button from "../../components/Button";
import GoBack from "../../components/GoBack";

import styles from "./styles";
import { useRouter } from "next/router";
import { useFormik } from "formik";
import { customerSchema } from "../../schemas";
import {
  normalizeCepNumber,
  normalizeCpfNumber,
  normalizePhoneNumber,
} from "../../lib/mask";
import Select from "../../components/Select";
import {
  genderOptions,
  residentOptions,
  streetOptions,
} from "../../lib/selectOptions";
import SuccessMsg from "../../components/SuccessMsg";
import { createCustomer } from "../../api/customer";

export default function RegisterAccount() {
  const router = useRouter();
  const [success, setSuccess] = useState(null);

  const onSubmit = async (values, actions) => {
    try {
      await createCustomer(values);
      setSuccess(true);
    } catch (error) {
      console.log("deu erro");
    }
  };

  const {
    values,
    errors,
    touched,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useFormik({
    initialValues: {
      customerName: "",
      customerDocumentNumber: "",
      customerBirthDate: "",
      customerGender: "",
      customerPhone: "",
      customerPassword: "",
      customerEmail: "",
      customerConfirmPassword: "",
      addressNickname: "",
      addressTypeResident: "",
      addressTypeStreet: "",
      addressStreet: "",
      addressNumber: "",
      addressNeighborhood: "",
      addressCity: "",
      addressState: "",
      addressCountry: "",
      addressZipCode: "",
      addressComments: "",
    },
    validationSchema: customerSchema,
    onSubmit,
  });

  if (success)
    return (
      <div className="success-container">
        <style jsx>{styles}</style>
        <SuccessMsg onClick={() => router.push("/")} />
      </div>
    );

  return (
    <div className="page-register-client">
      <GoBack router={router} />
      <style jsx>{styles}</style>
      <h1>Cadastro Cliente</h1>
      <form onSubmit={handleSubmit}>
        <article>
          <header>Dados Básicos</header>
          <div className="basic-data-inputs">
            <div className="row">
              <Input
                value={values.customerName}
                onChange={handleChange}
                onBlur={handleBlur}
                label="Nome Completo"
                name="customerName"
                errorMsg={touched.customerName ? errors.customerName : ""}
              />
            </div>
            <div className="row">
              <Input
                label="CPF"
                value={values.customerDocumentNumber}
                onChange={(e) => {
                  e.target.value = normalizeCpfNumber(e.target.value);
                  handleChange(e);
                }}
                onBlur={handleBlur}
                name="customerDocumentNumber"
                errorMsg={
                  touched.customerDocumentNumber
                    ? errors.customerDocumentNumber
                    : ""
                }
              />
              <Input
                label="Data de Nascimento"
                type="date"
                onChange={handleChange}
                onBlur={handleBlur}
                name="customerBirthDate"
                errorMsg={
                  touched.customerBirthDate ? errors.customerBirthDate : ""
                }
              />
            </div>
            <div className="row">
              <Select
                label={"Genêro"}
                name={"customerGender"}
                options={genderOptions}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.customerGender ? errors.customerGender : ""}
              />
              <Input
                label="Telefone"
                value={values.customerPhone}
                onChange={(e) => {
                  e.target.value = normalizePhoneNumber(e.target.value);
                  handleChange(e);
                }}
                onBlur={handleBlur}
                name="customerPhone"
                errorMsg={touched.customerPhone ? errors.customerPhone : ""}
              />
            </div>
          </div>
        </article>
        <article>
          <header>Endereço Residencial</header>
          <div className="residential-address-inputs">
            <div className="row">
              <Input
                label="Identificação do endereço (apelido)"
                name="addressNickname"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.addressNickname ? errors.addressNickname : ""}
              />
              <Select
                label="Tipo Residência"
                name="addressTypeResident"
                options={residentOptions}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={
                  touched.addressTypeResident ? errors.addressTypeResident : ""
                }
              />
            </div>
            <div className="row">
              <Select
                label="Tipo Logradouro"
                name="addressTypeStreet"
                options={streetOptions}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={
                  touched.addressTypeStreet ? errors.addressTypeStreet : ""
                }
              />
              <Input
                label="Logradouro"
                name="addressStreet"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.addressStreet ? errors.addressStreet : ""}
              />
              <Input
                label="Número"
                name="addressNumber"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.addressNumber ? errors.addressNumber : ""}
              />
            </div>
            <div className="row">
              <Input
                label="Bairro"
                name="addressNeighborhood"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={
                  touched.addressNeighborhood ? errors.addressNeighborhood : ""
                }
              />
              <Input
                label="Cidade"
                name="addressCity"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.addressCity ? errors.addressCity : ""}
              />
              <Input
                label="Estado"
                name="addressState"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.addressState ? errors.addressState : ""}
              />
              <Input
                label="País"
                name="addressCountry"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.addressCountry ? errors.addressCountry : ""}
              />
            </div>
            <div className="row">
              <Input
                label="CEP"
                name="addressZipCode"
                value={values.addressZipCode}
                onChange={(e) => {
                  e.target.value = normalizeCepNumber(e.target.value);
                  handleChange(e);
                }}
                onBlur={handleBlur}
                errorMsg={touched.addressZipCode ? errors.addressZipCode : ""}
              />
              <Input
                label="Observações"
                name="addressComments"
                onChange={handleChange}
                onBlur={handleBlur}
                errorMsg={touched.addressComments ? errors.addressComments : ""}
              />
            </div>
          </div>
        </article>
        <article>
          <header>Dados de Login</header>
          <div className="login-data-inptus">
            <Input
              name="customerEmail"
              label="E-mail"
              value={values.customerEmail}
              onChange={handleChange}
              onBlur={handleBlur}
              errorMsg={touched.customerEmail ? errors.customerEmail : ""}
            />
            <Input
              name="customerPassword"
              label="Senha"
              type="password"
              value={values.customerPassword}
              onChange={handleChange}
              onBlur={handleBlur}
              errorMsg={touched.customerPassword ? errors.customerPassword : ""}
            />
            <Input
              name="customerConfirmPassword"
              type="password"
              label="Confirme Senha"
              value={values.customerConfirmPassword}
              onChange={handleChange}
              onBlur={handleBlur}
              errorMsg={
                touched.customerConfirmPassword
                  ? errors.customerConfirmPassword
                  : ""
              }
            />
          </div>
        </article>
        <div className="container-button">
          <Button textButton="Cadastrar" disabled={isSubmitting} />
        </div>
      </form>
    </div>
  );
}
