import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import styles from "./styles";
import Counter from "../../components/Counter";
import Button from "../../components/Button";
import GoBack from "../../components/GoBack";
import { addShopCart, getProduct } from "../../api/products";
import dlv from "dlv";

export default function ViewProduct() {
  const [product, setProduct] = useState({});
  const [counter, setCounter] = useState(1);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  useEffect(() => {
    (async function () {
      try {
        const productBack = await getProduct(router.query.id);
        setProduct(productBack.data);
        console.log(productBack.data);
        setCounter(
          productBack.data.itemProductQuantity === 0
            ? 1
            : productBack.data.itemProductQuantity
        );
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  const handleAddShopCart = async () => {
    try {
      await addShopCart(router.query.id, counter - product.itemProductQuantity);
      router.push("/carrinho");
    } catch (error) {
      console.log(error);
    }
  };

  const handleClickCounter = (newCounter) => {
    setCounter(counter + newCounter);
  };

  if (loading) return <p>Carregando...</p>;

  return (
    <>
      <style jsx>{styles}</style>
      <GoBack router={router} />
      <main className="product-view">
        <section className="row">
          <img
            src={dlv(product, "itemProduct.productImageUrl")}
            alt={dlv(product, "itemProduct.productTitle")}
            className="image-container"
          />
          <section className="product-info">
            <section>
              <h1>{dlv(product, "itemProduct.productTitle")} Teste</h1>
              <article>
                <p className="info-describe">Editora</p>
                <p className="info-value">
                  {dlv(product, "itemProduct.productPublisher")}
                </p>
              </article>
              <article>
                <p className="info-describe">Páginas</p>
                <p className="info-value">
                  {dlv(product, "itemProduct.productPages")} páginas
                </p>
              </article>
              <article>
                <p className="info-describe">Autor(a)</p>
                <p className="info-value">
                  {dlv(product, "itemProduct.productAuthor")}
                </p>
              </article>
              <article>
                <p className="info-describe">Ano publicação</p>
                <p className="info-value">
                  {dlv(product, "itemProduct.productReleaseYear")}
                </p>
              </article>
            </section>
            <section className="actions">
              <div className="price-n-counter">
                <p className="price">
                  R$ {dlv(product, "itemProduct.productSale")}
                </p>
                <Counter
                  counter={counter}
                  onClick={handleClickCounter}
                  limit={dlv(product, "itemProduct.productQuantity")}
                />
              </div>
              <Button
                textButton="Adicionar ao Carrinho"
                onClick={() => handleAddShopCart()}
              />
            </section>
          </section>
        </section>
        <section className="product-sinopse">
          <h4>Sinopse</h4>
          <p>{dlv(product, "itemProduct.productSynopsis")}</p>
        </section>
      </main>
    </>
  );
}
