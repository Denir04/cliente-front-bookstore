import css from "styled-jsx/css";

export default css`
  .product-view {
    margin: 0 200px;
  }

  .product-view .row {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 30px;
  }

  .product-view .image-container {
    width: 350px;
    min-width: 350px;
    height: 500px;
    min-height: 500px;
    border: 1px solid var(--color-zero);
    margin: 0 auto;
  }

  .product-view .product-info {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 500px;
    border: 2px solid var(--color-zero);
    background-color: var(--color-secondary);
    padding: 15px;
  }

  .product-view .product-info article {
    margin: 15px 0;
    gap: 10px;
  }

  .product-view .product-info article .info-describe {
    font-weight: bold;
    margin-bottom: 5px;
  }

  .product-view .product-info article .info-value {
    font-size: 1.1rem;
  }

  .product-view .product-info .actions {
    display: flex;
    flex-direction: column;
    gap: 10px;
  }

  .product-view .product-info .actions .price-n-counter {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .product-view .product-info .actions .price-n-counter .price {
    font-size: 1.4rem;
  }

  .product-view .product-sinopse {
    width: 100%;
    height: fit-content;
    min-height: 150px;
    border: 2px solid var(--color-zero);
    background-color: var(--color-secondary);
    padding: 15px 30px;
  }

  .product-view .product-sinopse h4 {
    font-weight: bold;
    margin-bottom: 10px;
  }
`;
