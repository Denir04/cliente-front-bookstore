import * as yup from "yup";

const passwordRegex =
  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!?@#\$%\^&\*]).{8,}$/;

export const customerSchema = yup.object().shape({
  customerName: yup.string().required("Campo obrigatório"),
  customerDocumentNumber: yup
    .string()
    .min(13, "CPF inválido")
    .required("Campo obrigatório"),
  customerBirthDate: yup.date().required("Campo obrigatório"),
  customerGender: yup.string().required("Campo obrigatório"),
  customerPhone: yup
    .string()
    .min(13, "Telefone inválido")
    .required("Campo obrigatório"),
  addressNickname: yup
    .string()
    .required("Campo obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ0-9\s]+$/,
      "Este campo só pode receber letras, numeros e espaço"
    ),
  addressTypeResident: yup.string().required("Campo Obrigatório"),
  addressTypeStreet: yup.string().required("Campo Obrigatório"),
  addressStreet: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ.\s]+$/,
      "Este campo só pode receber letras, ponto e espaço"
    ),
  addressNumber: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ0-9]+$/,
      "Este campo só pode receber números e letras"
    ),
  addressNeighborhood: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressCity: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressState: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressCountry: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressZipCode: yup
    .string()
    .min(9, "CEP inválido")
    .required("Campo Obrigatório"),
  addressComments: yup
    .string()
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  customerEmail: yup
    .string()
    .email("Email inválido")
    .required("Campo obrigatório"),
  customerPassword: yup
    .string()
    .matches(passwordRegex, {
      message:
        "Mínimo 8 caracteres, sendo pelos menos 1 minúsculo, 1 maiúsculo e 1 caracter especial",
    })
    .required("Campo obrigatório"),
  customerConfirmPassword: yup
    .string()
    .oneOf([yup.ref("customerPassword"), null], "Senhas precisam ser iguais")
    .required("Campo obrigatório"),
});

export const basicCustomerSchema = yup.object().shape({
  customerName: yup.string().required("Campo obrigatório"),
  customerDocumentNumber: yup
    .string()
    .min(13, "CPF inválido")
    .required("Campo obrigatório"),
  customerBirthDate: yup.date().required("Campo obrigatório"),
  customerGender: yup.string().required("Campo obrigatório"),
  customerPhone: yup
    .string()
    .min(13, "Telefone inválido")
    .required("Campo obrigatório"),
  customerEmail: yup
    .string()
    .email("Email inválido")
    .required("Campo obrigatório"),
});

export const passwordsSchema = yup.object().shape({
  customerPassword: yup
    .string()
    .matches(passwordRegex, {
      message:
        "Mínimo 8 caracteres, sendo pelos menos 1 minúsculo, 1 maiúsculo e 1 caracter especial",
    })
    .required("Campo obrigatório"),
  customerConfirmPassword: yup
    .string()
    .oneOf([yup.ref("customerPassword"), null], "Senhas precisam ser iguais")
    .required("Campo obrigatório"),
});

export const addressSchema = yup.object().shape({
  addressNickname: yup
    .string()
    .required("Campo obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ0-9\s]+$/,
      "Este campo só pode receber letras, numeros e espaço"
    ),
  addressTypeResident: yup.string().required("Campo Obrigatório"),
  addressTypeStreet: yup.string().required("Campo Obrigatório"),
  addressStreet: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ.\s]+$/,
      "Este campo só pode receber letras, ponto e espaço"
    ),
  addressNumber: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ0-9]+$/,
      "Este campo só pode receber números e letras"
    ),
  addressNeighborhood: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressCity: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressState: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressCountry: yup
    .string()
    .required("Campo Obrigatório")
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
  addressZipCode: yup
    .string()
    .min(9, "CEP inválido")
    .required("Campo Obrigatório"),
  addressComments: yup
    .string()
    .matches(
      /^[A-Za-zÀ-ÖØ-öø-ÿ\s]+$/,
      "Este campo só pode receber letras e espaço"
    ),
});

export const creditCardSchema = yup.object({
  cardNumber: yup
    .string()
    .min(19, "Numero de cartão inválido")
    .required("Campo Obrigatório"),
  cardName: yup.string().required("Campo Obrigatório"),
  cardSecurityCode: yup
    .string()
    .min(3, "Código de segurança inválido")
    .required(),
  cardBrand: yup.string().required("Campo Obrigatório"),
});
