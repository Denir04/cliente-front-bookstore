import { api } from ".";

export const startOrder = () => {
  return api.post(`/orders/startorder`);
};

export const getSummary = () => {
  return api.get(`/orders/summary`);
};

export const sendAddressDelivery = (customerId, addressId) => {
  return api.post(`/orders/shipping/${customerId}/address/${addressId}`);
};

export const sendPaymentWay = (creditCards) => {
  return api.post(`/orders/paymentcards`, creditCards);
};

export const finishOrder = () => {
  return api.post(`/orders/finishorder`);
};

export const getMyOrders = (customerId) => {
  return api.get(`/orders/customers/${customerId}/orders`);
};

export const getMyOrderById = (customerId, orderId) => {
  return api.get(`/orders/customers/${customerId}/orders/${orderId}`);
};
