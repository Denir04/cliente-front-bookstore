import { api } from ".";
import { normalizeAddress, normalizeCustomer } from "../lib/normalize";

export const createCustomer = (customer) => {
  const normalizedCustomer = normalizeCustomer(customer);
  return api.post("/customers", normalizedCustomer);
};

export const getCustomer = (id) => {
  return api.get(`/customers/${id}`);
};

export const getActivedCustomers = () => {
  return api.get(`/customers/actived`);
};

export const updateCustomer = (id, newDatas) => {
  return api.patch(`/customers/${id}`, newDatas);
};

export const getCustomerAddresses = (id) => {
  return api.get(`/customers/${id}/addresses`);
};

export const getCustomerAddress = (customerId, addressId) => {
  return api.get(`/customers/${customerId}/addresses/${addressId}`);
};

export const deleteCustomerAddress = (customerId, addressId) => {
  return api.delete(`/customers/${customerId}/addresses/${addressId}`);
};

export const createNewAddress = (customerId, newAddress) => {
  return api.post(`/customers/${customerId}/addresses`, newAddress);
};

export const updateAddress = (customerId, addressId, updatedAddress) => {
  return api.put(
    `/customers/${customerId}/addresses/${addressId}`,
    updatedAddress
  );
};

export const createCreditCard = (customerId, newCreditCard) => {
  return api.post(`/customers/${customerId}/cards`, {
    ...newCreditCard,
    cardPreference: false,
  });
};

export const getCreditCards = (customerId) => {
  return api.get(`/customers/${customerId}/cards`);
};

export const deleteCreditCard = (customerId, cardId) => {
  return api.delete(`/customers/${customerId}/cards/${cardId}`);
};

export const updateCreditCard = (customerId, cardId) => {
  return api.put(`customers/${customerId}/cards/${cardId}/preference`);
};
