import { api } from ".";

export const getAllProducts = () => {
  return api.get(`/products`);
};

export const getProduct = (id) => {
  return api.get(`/cart/${id}`);
};

export const getShopCart = () => {
  return api.get(`/cart`);
};

export const addShopCart = (id, quantity = 1) => {
  return api.post(`/cart`, {
    itemProductId: id,
    itemProductQuantity: quantity,
  });
};

export const removeShopCart = (id) => {
  console.log(id);
  return api.delete(`/cart/${id}`);
};
