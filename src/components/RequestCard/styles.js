import css from "styled-jsx/css";

export default css`
  .request-card {
    border: 1px solid var(--color-zero);
    width: 100%;
    height: fit-content;
    background-color: var(--color-secondary);
  }

  .request-card .header {
    border-bottom: 1px solid var(--color-zero);
    height: 35px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 20px;
  }

  .request-card .header .status {
    font-size: 1.1rem;
    font-weight: bold;
  }

  .request-card .content {
    display: flex;
    gap: 20px;
    padding: 20px 20px;
    height: 50px;
  }

  .request-card .content p {
    text-align: center;
  }

  .request-card .content .view-request {
    margin-left: auto;
    cursor: pointer;
  }
`;
