import React from "react";
import styles from "./styles";

export default function RequestCard({
  id = "000000000000",
  status = "Em Processamento",
  requestDate = "XX/XX/XXXX",
  itemsValue = null,
  totalValue = "00,00",
  onView = () => {},
}) {
  return (
    <article className="request-card">
      <style jsx>{styles}</style>
      <header className="header">
        <p>N° Pedido Compra #{id}</p>
        <p className="status">{status}</p>
      </header>
      <div className="content">
        <p>
          Data do Pedido <br />
          {requestDate}
        </p>
        <p className="view-request" onClick={onView}>
          Visualizar
        </p>
      </div>
    </article>
  );
}
