import { css } from "styled-jsx/css";

export default css`
  .address-card {
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 25px;
    padding: 10px 15px;
    border-radius: 5px;
    background-color: var(--color-secondary);
    border: 1px solid var(--color-zero);
    min-height: 80px;
  }

  .address-card input {
    width: 25px;
    height: 25px;
  }

  .address-card .address-info h3 {
    font-size: 1.2rem;
    font-weight: bold;
  }

  .address-card .address-info p {
    font-size: 1rem;
  }

  .address-card .address-info {
    user-select: none;
    display: flex;
    flex-direction: column;
    gap: 5px;
  }

  .address-card .address-info .address-tags {
    display: flex;
    justify-content: flex-start;
    gap: 20px;
    margin-top: auto;
  }

  .address-card .address-info .address-tags span {
    font-size: 0.9rem;
    padding: 3px 15px;
    color: var(--color-secondary);
    background-color: var(--color-primary);
    border-radius: 4px;
  }

  .address-card .address-actions {
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 20px;
  }

  .address-card .address-actions .edit {
    color: var(--color-forth);
    cursor: pointer;
  }

  .address-card .address-actions .delete {
    color: var(--color-terciary);
    cursor: pointer;
  }
`;
