import React from "react";
import styles from "./styles";
import { streetOptions } from "../../lib/selectOptions";
import dlv from "dlv";

export default function AddressCard({
  nickname = "teste",
  id = "teste",
  typeStreet = "Rua",
  neighborhood = "teste",
  street = "teste",
  number = 100,
  defaultBilling = false,
  defaultDelivery = false,
  defaultResident = false,
  router = () => {},
}) {
  const typeStreetOption = streetOptions.find(
    (option) => option.value === typeStreet
  );

  return (
    <article className="address-card">
      <style jsx>{styles}</style>
      <div className="address-info">
        <h3>{nickname}</h3>
        <p>
          {neighborhood},{dlv(typeStreetOption, "name")} {street}, {number}
        </p>
        <div className="address-tags">
          {defaultResident && <span>Residencial</span>}
          {defaultBilling && <span>Cobrança</span>}
          {defaultDelivery && <span>Entrega</span>}
        </div>
      </div>
      <div className="address-actions">
        <p
          className="edit"
          onClick={() => router.push(`/conta/meus-enderecos/editar/${id}`)}
        >
          Editar
        </p>
        <p
          className="delete"
          onClick={() => router.push(`/conta/meus-enderecos/excluir/${id}`)}
        >
          Excluir
        </p>
      </div>
    </article>
  );
}
