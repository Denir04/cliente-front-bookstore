import React from "react";
import styles from "./styles";

export default function IntervarInput() {
  return (
    <div className="interval-input">
      <style jsx>{styles}</style>
      <input type="text" placeholder="Mínimo" />
      <span>-</span>
      <input type="text" placeholder="Máximo" />
    </div>
  );
}
