import css from "styled-jsx/css";

export default css`
  .interval-input {
    display: flex;
    align-items: center;
    width: 100%;
  }

  .interval-input input {
    width: 30%;
  }

  .interval-input span {
    margin: 0 5px;
  }
`;
