import css from "styled-jsx/css";

export default css`
  .counter-component {
    width: 150px;
    height: 30px;
    display: flex;
    align-items: center;
    gap: 10px;
  }

  .counter-component button {
    width: 33%;
    height: 100%;
  }

  .counter-component .counter-value {
    width: 33%;
    height: 100%;
    display: block;
    background-color: var(--color-base);
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
