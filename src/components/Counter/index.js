import React, { useState } from "react";
import styles from "./styles";
import { addShopCart } from "../../api/products";

export default function Counter({ counter, limit, onClick }) {
  return (
    <div className="counter-component">
      <style jsx>{styles}</style>
      <button
        className="counter-minus"
        disabled={counter <= 1}
        onClick={() => onClick(-1)}
      >
        -
      </button>
      <div className="counter-value">{counter}</div>
      <button
        disabled={counter >= limit}
        className="counter-plus"
        onClick={() => onClick(1)}
      >
        +
      </button>
    </div>
  );
}
