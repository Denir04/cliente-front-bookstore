import React from "react";
import Image from "next/image";

import Button from "../Button";
import successIcon from "../../static/feito.png";
import styles from "./styles";

export default function SuccessMsg({
  message = "Operação foi um Sucesso",
  onClick = () => {},
}) {
  return (
    <div className="success-component">
      <style jsx>{styles}</style>
      <div className="row">
        <div className="icon-container">
          <Image src={successIcon} alt="Successo" fill priority />
        </div>
        <p>{message}</p>
      </div>
      <div className="button-container">
        <Button textButton="OK" className="secondary" onClick={onClick} />
      </div>
    </div>
  );
}
