import css from "styled-jsx/css";

export default css`
  .success-component {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 10px;
  }

  .success-component .row {
    display: flex;
    gap: 10px;
    align-items: center;
  }

  .success-component .icon-container {
    position: relative;
    width: 50px;
    height: 50px;
  }

  .success-component .button-container {
    width: 100px;
  }
`;
