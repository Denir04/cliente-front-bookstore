import React from "react";
import styles from "./styles";

export default function Checkbox({
  id = "Id",
  label = "Default Label",
  ...props
}) {
  return (
    <div className="checkbox-component">
      <style jsx>{styles}</style>
      <input type="checkbox" id={id} {...props} />
      <label htmlFor={id}>{label}</label>
    </div>
  );
}
