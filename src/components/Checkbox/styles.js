import css from "styled-jsx/css";

export default css`
  .checkbox-component {
    display: flex;
    align-items: center;
    gap: 10px;
    cursor: pointer;
    width: fit-content;
  }

  .checkbox-component input {
    width: 20px;
    height: 20px;
    cursor: inherit;
    margin: 0;
  }

  .checkbox-component label {
    user-select: none;
    cursor: inherit;
  }
`;
