import React from "react";
import Link from "next/link";
import styles from "./styles";
import Image from "next/image";

import imgShopCart from "../../static/carrinho-de-compras.png";
import imgProfile from "../../static/profile.png";

export default function Header() {
  return (
    <header className="header-component">
      <style jsx>{styles}</style>
      <Link href="/home">
        <p className="logo">
          Book<span>Store</span>
        </p>
      </Link>
      <div className="icons">
        <Link href="/cadastro">
          <p className="create-account">Cadastro Cliente</p>
        </Link>
        <Link href="/listar-clientes">
          <p className="create-account">Clientes Ativos</p>
        </Link>
        <Link href="/conta/minha-conta">
          <div className="icon-container">
            <Image src={imgProfile} alt="Ir Conta" fill />
          </div>
        </Link>
        <Link href="/carrinho">
          <div className="icon-container">
            <Image src={imgShopCart} alt="Ir Carrinho de Compras" fill />
          </div>
        </Link>
      </div>
    </header>
  );
}
