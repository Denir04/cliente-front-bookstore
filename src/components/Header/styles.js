import css from "styled-jsx/css";

export default css`
  .header-component {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 25px;
    background-color: var(--color-primary);
    height: 9vh;
  }

  .header-component .logo {
    color: var(--color-base);
    font-size: 1.4rem;
  }

  .header-component .logo span {
    font-weight: bold;
  }

  .header-component .icons {
    display: flex;
    align-items: center;
    gap: 20px;
  }

  .create-account {
    cursor: pointer;
    color: var(--color-zero);
  }

  .header-component .icon-container {
    position: relative;
    width: 35px;
    height: 35px;
  }
`;
