import React from "react";
import styles from "./styles";

export default function GoBack({ router }) {
  return (
    <>
      <style jsx>{styles}</style>
      <p className="go-back" onClick={() => router.back()}>
        <span>&lt;</span> Voltar
      </p>
    </>
  );
}
