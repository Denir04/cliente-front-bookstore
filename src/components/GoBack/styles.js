import css from "styled-jsx/css";

export default css`
  .go-back {
    display: flex;
    align-items: center;
    color: var(--color-zero);
    cursor: pointer;
    width: fit-content;
    margin-bottom: 15px;
  }

  .go-back span {
    font-size: 1.8rem;
  }
`;
