import css from "styled-jsx/css";

export default css`
  .product-shop-card {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    align-items: center;
    text-align: center;
    margin-bottom: 20px;
  }

  .product-shop-card .image-container {
    margin: 0 auto;
    position: relative;
    width: 80px;
    height: 120px;
  }

  .product-shop-card .title {
    text-align: left;
    font-weight: bold;
  }

  .product-shop-card .remove {
    color: red;
    cursor: pointer;
  }
`;
