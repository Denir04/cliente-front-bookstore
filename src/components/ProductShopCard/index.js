import React, { useState } from "react";

import styles from "./styles";
import Counter from "../Counter";
import { addShopCart } from "../../api/products";
import Link from "next/link";

export default function ProductShopCard({
  imgUrl = "/",
  productId = null,
  title = "default title",
  price = "0,00",
  quantity = 0,
  limit = 0,
  onRemove = () => {},
  changedCounter = true,
  onChangedCounter = () => {},
}) {
  const [counter, setCounter] = useState(quantity);

  const handleClickCounter = async (insertNum) => {
    try {
      await addShopCart(productId, insertNum);
      setCounter(counter + insertNum);
      onChangedCounter(!changedCounter);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <article className="product-shop-card">
      <style jsx>{styles}</style>
      <Link href={`/produto/${productId}`}>
        <img className="image-container" src={imgUrl} alt={title} />
      </Link>
      <p className="title">{title}</p>
      <Counter counter={counter} onClick={handleClickCounter} limit={limit} />
      <p>R$ {price}</p>
      <p className="remove" onClick={() => onRemove(productId)}>
        Remover
      </p>
    </article>
  );
}
