import css from "styled-jsx/css";

export default css`
  .selected-product-card {
    display: flex;
    gap: 8px;
  }

  .selected-product-card .image-container {
    width: 90px;
    height: 110px;
  }

  .selected-product-card .image-container img {
    width: 100%;
    height: 100%;
  }

  .selected-product-card .product-info {
    display: flex;
    flex-direction: column;
  }

  .selected-product-card .product-info .amount {
    font-size: 0.9rem;
  }

  .selected-product-card .product-info .price {
    margin-top: auto;
    font-weight: bold;
    font-size: 1.1rem;
  }
`;
