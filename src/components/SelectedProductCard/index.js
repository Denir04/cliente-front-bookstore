import React from "react";
import Image from "next/image";

import styles from "./styles";

export default function SelectedProductCard({
  imgUrl = "/",
  price = "0,00",
  amount = 1,
}) {
  return (
    <article className="selected-product-card">
      <style jsx>{styles}</style>
      <div className="image-container">
        <img src={imgUrl} alt={"produto figura"} />
      </div>
      <div className="product-info">
        <p className="amount">Quantidade: {amount}</p>
        <p className="price">R$ {price}</p>
      </div>
    </article>
  );
}
