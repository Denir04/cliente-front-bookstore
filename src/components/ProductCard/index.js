import React from "react";
import Link from "next/link";

import styles from "./styles";

export default function ProductCard({
  title = "Produto Generico",
  price = "0,00",
  imageUrl = "",
  id = "",
}) {
  return (
    <Link href={`/produto/${id}`}>
      <article className="product-card">
        <style jsx>{styles}</style>
        <img
          src={imageUrl}
          alt="Pequeno Principe"
          className="image-container"
        />
        <div className="product-info">
          <p className="title">{title}</p>
          <p className="price">R$ {price}</p>
        </div>
      </article>
    </Link>
  );
}
