import css from "styled-jsx/css";

export default css`
  .product-card {
    border: 2px solid var(--color-zero);
    border-radius: 3px;
    width: 200px;
    padding: 10px 15px;
    background-color: var(--color-secondary);
  }

  .product-card .image-container {
    width: 100%;
    min-width: 100%;
    height: 250px;
    min-height: 250px;
    border: 1px solid var(--color-zero);
  }

  .product-card .product-info {
    color: var(--color-zero);
    margin-top: 10px;
  }

  .product-card .product-info .title {
    margin-bottom: 5px;
  }

  .product-card .product-info .price {
    font-size: 1.1rem;
    font-weight: bold;
  }
`;
