import css from "styled-jsx/css";

export default css`
  .input-component {
    display: flex;
    flex-direction: column;
  }

  .input-error {
    border: 2px solid red;
  }

  span {
    margin-top: 3px;
    color: red;
    font-size: 12px;
  }
`;
