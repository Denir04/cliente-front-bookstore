import React from "react";
import styles from "./styles";

export default function Input({
  label,
  type = "text",
  errorMsg = "",
  ...props
}) {
  return (
    <div className="input-component">
      <style jsx>{styles}</style>
      <label>{label}</label>
      <input type={type} {...props} className={errorMsg ? "input-error" : ""} />
      {errorMsg && <span>{errorMsg}</span>}
    </div>
  );
}
