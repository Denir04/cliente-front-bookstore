import React from "react";
import styles from "./styles";
import { streetOptions } from "../../lib/selectOptions";
import dlv from "dlv";

export default function SelectAddressCard({
  addressId = 1,
  defaultAddress = false,
  selected = false,
  typeStreet = "Rua",
  nickname = "Default",
  neighborhood = "Default",
  street = "Rua",
  number = 100,
  onChange = () => {},
}) {
  const typeStreetOption = streetOptions.find(
    (option) => option.value === typeStreet
  );

  return (
    <article className="select-address-card">
      <style jsx>{styles}</style>
      {!selected && (
        <input
          type="radio"
          name="selectedName"
          value={addressId}
          id={addressId}
          onChange={onChange}
        />
      )}
      <label className="address-info" htmlFor={addressId}>
        <h3>{nickname}</h3>
        <p>
          {neighborhood},{dlv(typeStreetOption, "name")} {street}, {number}
        </p>
        <div className="address-tags">
          {defaultAddress && <span>Padrão</span>}
        </div>
      </label>
    </article>
  );
}
