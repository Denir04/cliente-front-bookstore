import css from "styled-jsx/css";

export default css`
  .select-address-card {
    display: flex;
    justify-content: flex-start;
    gap: 25px;
    margin: 18px 0;
  }

  .select-address-card input {
    width: 25px;
    height: 25px;
  }

  .select-address-card .address-info h3 {
    font-size: 1.2rem;
    font-weight: bold;
  }

  .select-address-card .address-info p {
    font-size: 0.8rem;
  }

  .select-address-card .address-info {
    user-select: none;
    display: flex;
    flex-direction: column;
    gap: 5px;
  }

  .select-address-card .address-info .address-tags {
    display: flex;
    justify-content: flex-start;
    gap: 20px;
  }

  .select-address-card .address-info .address-tags span {
    font-size: 0.9rem;
    padding: 3px 15px;
    color: var(--color-secondary);
    background-color: var(--color-primary);
    border-radius: 4px;
  }
`;
