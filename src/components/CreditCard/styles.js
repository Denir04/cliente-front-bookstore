import css from "styled-jsx/css";

export default css`
  .credit-card {
    display: flex;
    justify-content: space-between;
    align-items: center;
    border: 1px solid var(--color-zero);
    background-color: var(--color-secondary);
    padding: 10px 15px;
    min-height: 70px;
    border-radius: 5px;
  }

  .credit-card .flag-n-last-numbers {
    margin-left: 20px;
    font-size: 1.1rem;
    font-weight: bold;
    user-select: none;
  }

  .credit-card .actions {
    display: flex;
    gap: 20px;
  }

  .credit-card .preferential-field {
    min-width: 300px;
    text-align: center;
  }

  .credit-card .preferential-field span {
    font-size: 0.9rem;
    padding: 3px 15px;
    color: var(--color-secondary);
    background-color: var(--color-primary);
    border-radius: 4px;
  }

  .credit-card .preferential-field .edit-card {
    color: var(--color-primary);
    cursor: pointer;
    font-weight: 600;
  }

  .credit-card .delete {
    cursor: pointer;
    color: var(--color-terciary);
  }
`;
