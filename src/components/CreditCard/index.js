import React from "react";

import styles from "./styles";

export default function CreditCard({
  id = 1,
  brand = "default",
  lastNumbers = "0000",
  name = "default",
  preferential = false,
  router = () => {},
}) {
  return (
    <article className="credit-card">
      <style jsx>{styles}</style>
      <label className="flag-n-last-numbers" htmlFor={name}>
        {brand} **** {lastNumbers.substring(lastNumbers.length - 4)}
      </label>
      <div className="actions">
        <div className="preferential-field">
          {preferential ? (
            <span>Preferencial</span>
          ) : (
            <p
              className="edit-card"
              onClick={() => router.push(`/conta/meus-cartoes/editar/${id}`)}
            >
              Configurar como preferencial
            </p>
          )}
        </div>
        <p
          className="delete"
          onClick={() => router.push(`/conta/meus-cartoes/excluir/${id}`)}
        >
          Excluir
        </p>
      </div>
    </article>
  );
}
