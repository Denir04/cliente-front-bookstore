import css from "styled-jsx/css";

export default css`
  button {
    width: 100%;
    border: none;
    border-radius: 3px;
    padding: 3px 8px;
    font-size: inherit;
    cursor: pointer;
  }

  .primary {
    background-color: var(--color-btn-primary);
    color: var(--color-secondary);
  }

  .secondary {
    background-color: var(--color-btn-secondary);
    color: var(--color-zero);
  }

  .terciary {
    background-color: var(--color-terciary);
    color: var(--color-secondary);
  }

  .forth {
    background-color: var(--color-primary);
    color: var(--color-secondary);
  }

  button:disabled {
    opacity: 0.3;
  }
`;
