import React from "react";
import styles from "./styles";

export default function Button({
  type = "submit",
  textButton = "Default",
  className = "primary",
  onClick = () => {},
  ...props
}) {
  return (
    <>
      <style jsx>{styles}</style>
      <button type={type} className={className} onClick={onClick} {...props}>
        {textButton}
      </button>
    </>
  );
}
