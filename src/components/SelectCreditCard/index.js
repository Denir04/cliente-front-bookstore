import React, { useState } from "react";

import styles from "./styles";

export default function SelectCreditCard({
  id = 0,
  selected = false,
  brand = "default",
  lastNumbers = "0000",
  name = "default",
  creditValue = "",
  onChangeBox = () => {},
  onChangeValue = () => {},
}) {
  const [checked, setChecked] = useState(false);
  const [value, setValue] = useState(creditValue);

  return (
    <article className="select-credit-card">
      <style jsx>{styles}</style>
      {!selected && (
        <input
          type="checkbox"
          name={name}
          id={name}
          checked={checked}
          onChange={() => {
            setChecked(!checked);
            onChangeBox(id);
            setValue(0);
          }}
        />
      )}
      <label className="flag-n-last-numbers" htmlFor={name}>
        {brand} **** {lastNumbers}
      </label>
      {(checked || selected) && (
        <div className="insert-values">
          <label htmlFor="value">Valor no Cartão</label>
          <input
            type="number"
            id="value"
            value={value}
            onChange={(e) => {
              setValue(e.target.value);
              onChangeValue(id, e.target.value);
            }}
            disabled={selected ? true : false}
          />
        </div>
      )}
    </article>
  );
}
