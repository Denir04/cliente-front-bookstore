import css from "styled-jsx/css";

export default css`
  .select-credit-card {
    display: flex;
    align-items: center;
    margin: 8px 0;
  }

  .select-credit-card input[type="checkbox"] {
    width: 25px;
    height: 25px;
    cursor: pointer;
  }

  .select-credit-card .flag-n-last-numbers {
    margin-left: 20px;
    font-size: 1.1rem;
    font-weight: bold;
    user-select: none;
  }

  .select-credit-card .insert-values {
    margin-left: auto;
    display: flex;
    align-items: center;
    gap: 15px;
  }
`;
