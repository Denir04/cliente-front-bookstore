import React from "react";
import styles from "./styles";

export default function Select({
  label,
  name,
  options = [],
  errorMsg,
  ...props
}) {
  return (
    <div className="select-component">
      <style jsx>{styles}</style>
      <label>{label}</label>
      <select name={name} {...props} className={errorMsg ? "select-error" : ""}>
        <option key="none" value="">
          Não definido
        </option>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.name}
          </option>
        ))}
      </select>
      {errorMsg && <span>{errorMsg}</span>}
    </div>
  );
}
