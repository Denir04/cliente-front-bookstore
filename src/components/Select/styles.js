import css from "styled-jsx/css";

export default css`
  .select-component {
    display: flex;
    flex-direction: column;
  }

  .select-component select {
    padding: 1px 2px;
  }

  .select-error {
    border: 2px solid red;
  }

  span {
    margin-top: 3px;
    color: red;
    font-size: 12px;
  }
`;
